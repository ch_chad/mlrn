#ifndef OBJDESCRIPTION_STR_H
#define OBJDESCRIPTION_STR_H

#include "ObjDescription.h"
#include <vector>

class ObjDescription_str : public ObjDescription
{
    public:
        ObjDescription_str();
        ~ObjDescription_str();
//        template <>
            unsigned int addObject(std::string desc);
        virtual std::string getObject_str(unsigned int obj);
        virtual std::string getValue(unsigned int obj, unsigned int attr);
        virtual unsigned int addObject(ObjDescription * orig_set, unsigned int orig_index);//copy
        virtual unsigned int deleteObject(unsigned int index);
        virtual unsigned int size();
    protected:
    private:
        std::vector <std::string> m_descr;
};

#endif // OBJDESCRIPTION_STR_H
