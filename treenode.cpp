#include "treenode.h"
#include <iostream>
#include <cmath>
treenode::treenode(std::string v):value(v),enc(0,0) {}
/*int treenode::disconnectChild(treenode * c)
{
    if (fchild==NULL)
    {
        std::cerr << "Error deleting child node: no children at all" <<"\r\n";
        return 1;
    }
    if (fchild==c)
    {
        if (value!="") std::cerr << "Warning: disconnecting child from a non-virtual node. Real parent already assigned at some time. Is encoding file correct?" << "\r\n";
        fchild=c->bro;
        c->bro=NULL;
        return 0;
    }
    treenode * ptr=fchild->bro, * prevptr=fchild;
    while (ptr!=NULL)
    {
        if (c==ptr)
        {
            if (value!="") std::cerr << "Warning: disconnecting child from a non-virtual node. Real parent already assigned at some time. Is encoding file correct?" << "\r\n";
            prevptr->bro=ptr->bro;
            return 0;
        }
        prevptr=ptr;
        ptr=ptr->bro;
    }
    std::cerr << "Child node to be deleted was not found" << "\r\n";
    return 1;
}*/
int treenode::orphanize(treenode * c)
{
    if (fchild==NULL)
    {
        return 1;
    }
    if (fchild==c)
    {
        if (value!="") std::cerr << "Warning: disconnecting child from a non-virtual node. Real parent already assigned at some time. Is encoding file correct?" << "\r\n";
        fchild=c->bro;
        c->bro=NULL;
        return 0;
    }
    treenode * ptr=fchild->bro, * prevptr=fchild;
    while (ptr!=NULL)
    {
        if (c==ptr)
        {
            if (value!="") std::cerr << "Warning: disconnecting child from a non-virtual node. Real parent already assigned at some time. Is encoding file correct?" << "\r\n";
            prevptr->bro=ptr->bro;
            return 0;
        }
        prevptr=ptr;
        ptr=ptr->bro;
    }
    ptr=fchild;
    while (ptr!=NULL)
    {
        if (ptr->orphanize(c) == 0) return 0;
        ptr=ptr->bro;
    }
    std::cerr << "Child node to be deleted was not found" << "\r\n";
    return 1;
}

treenode * treenode::addChild(treenode * c)
{
    #if OUTPUT_LEVEL>=ULTRA_OUTPUT
        std::clog << "Adding '" << c->value << "' to '" << value <<"'" << "\r\n";
    #endif
    if (c==this)
    {
        std::cerr << "Error parsing encoding file: '" << c->value << "' value is his own parent." << "\r\n";//TODO: look for duplicates in all other encoding generators!
        return NULL;
    }
    if (c->value == value) std::cerr << "Warning: duplicate value '" << value << "'" << "\r\n";
    if (fchild==NULL)
    {
        fchild=c;
        return c;
    }
    treenode * ptr=fchild;
    while (ptr->bro!=NULL)
    {
        if (ptr->value==c->value)
        {
            std::cerr << "Attempt to add a clone of already existing child '" << c->value << "' to '" << value << "'" << "\r\n";
            return ptr;
        }
        ptr=ptr->bro;
    }
    ptr->bro=c;
    return c;
}

std::string treenode::toString(size_t tabs_qtty)
{
    std::string tabs="";
    for (size_t i=0; i<tabs_qtty; i++) tabs+="    ";
    std::string enc_str;
    boost::to_string(enc, enc_str);
    std::string res=tabs+value+" ("+enc_str+"), ";
    res+=std::to_string((int)leavesHereAndBelow)+" nodes here and below"+"\n";
    std::clog << res;
    res="";
    treenode * ptr=fchild;
    while (ptr!=NULL)
    {
        //std::clog << "'" << value << "'s child '" << ptr->value << "' recursive call" << "\r\n";
        res+=ptr->toString(tabs_qtty+1);
        ptr=ptr->bro;
    }
    return res;
}
treenode * treenode::findInDescendants(std::string obj)
{//very clumsy, but to hell with this for now
    if (obj==value) return this;
    treenode * res=NULL;
    if (bro!=NULL) res=bro->findInDescendants(obj);
    if (res!=NULL) return res;
    if (fchild!=NULL) return fchild->findInDescendants(obj);
    return NULL;
}

size_t treenode::countLeaves()
{
    if (fchild==NULL)
    {
        leavesHereAndBelow=1;
    }
    else
    {
        treenode * ptr=fchild;
        while (ptr!=NULL)
        {
            leavesHereAndBelow+=ptr->countLeaves();
            ptr=ptr->bro;
        }
        leavesHereAndBelow++;
    }
    return leavesHereAndBelow;
}

void treenode::generateEncodings(size_t bits, std::vector<std::string> &bit_descriptors)
{
//    std::clog << "Started generating tree encoding" << "\r\n";
    countLeaves();
    //std::clog << this->toString() << "\r\n";
    enc=boost::dynamic_bitset<>(bits, 0);
    generateEncodings_internal(0, "", this, bit_descriptors);
}

void treenode::generateEncodings_internal(size_t offset_base, std::string tabs, treenode * root, std::vector<std::string> &bit_descriptors)//offset can be calculated actually, but that would be obsolete. last 1 in dbs is the last symbol of parent
{
    size_t offset=offset_base;
    treenode * ptr=fchild;
    std::string enc_str;
    boost::to_string(enc, enc_str);
    size_t enclen=enc.size();
//    std::clog << tabs << "current: " << value << " (" << enc_str << ")" << "\r\n";
    while (ptr!=NULL)
    {
        ptr->enc=boost::dynamic_bitset<>(enc);
        ptr->enc[enclen-offset-1]=1;
        bit_descriptors[enclen-offset-1]=ptr->value;
  //      std::clog << enclen-offset-1  << " " << ptr->value << "\r\n";
        root->names.push_back(std::string (ptr->value));
        //std::clog << tabs << "names length: " << root->names.size() << "\r\n";
        //      boost::to_string(ptr->enc, enc_str);
        //      std::clog << tabs << "child" << ptr->value << " (" << enc_str << ")" << "\r\n";
        ptr->generateEncodings_internal(offset+1, tabs+"    ", root, bit_descriptors);
        offset+=ptr->leavesHereAndBelow;
        //std::clog << tabs << offset << " offset (+" << ptr->leavesHereAndBelow << " from last child)" << "\r\n";
        ptr=ptr->bro;
    }
}
