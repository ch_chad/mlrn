#include "Attribute.h"
Attribute::Attribute():name("UNNAMED"), first_bit(0), last_bit(0)
{
    m_enctable.push_back(std::pair<std::string, std::string> ("NONE", "0"));
}
Attribute::Attribute(Attribute* orig):name(orig!=NULL?orig->name:""),
                                    first_bit(orig!=NULL?orig->first_bit:0),
                                    last_bit(orig!=NULL?orig->last_bit:0)
{
    if (orig==NULL)
    {
        std::cerr << "No original attribute provided" << "\r\n";
        m_enctable.clear();
        m_enctable.push_back(std::pair <std::string, std::string> ("NONE", "0"));
    }
    else
        m_enctable=std::vector<std::pair <std::string, std::string> > (orig->m_enctable);
}


void Attribute::printout(std::fstream *fs=NULL)
{
    (fs==NULL?std::clog:*fs) << "\r\n" << (target?"TARGET ":"") << attrname() << " (" << firstBit() << "," << lastBit() << ")\r\n";

    for (size_t i=0; i<m_enctable.size(); i++)
    {
        (fs==NULL?std::clog:*fs) << (m_enctable[i].first) << " -> " << (m_enctable[i].second) << "\r\n";
    }
    (fs==NULL?std::clog:*fs) << "\r\n";
}

int Attribute::genReplacementTable(std::string v_none, size_t start_bit)//for copied only
{
    //dont forget to copy bit descriptors!
    first_bit=start_bit;
    last_bit=start_bit+encLen();
    setOuter(0, v_none);
    return 0;
}

int Attribute::genReplacementTable(std::string type, std::string v_none, std::vector<std::string> values, size_t offset, size_t start_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors)
{
    //CAREFUL! i didnt want to create separate string vector, so it's not a clean vector of values, but other fields as well!
    //IMPORTANT: none value always exists. It's "000...0". Space always reserved.

    //must always create a table! the attribute is registered!
    while (bit_descriptors.size()<values.size()-offset+start_bit) bit_descriptors.push_back("");
    //std::clog << bit_descriptors.size() << "\r\n";
    if (type == "selective")
        gen_selective(values, offset, start_bit, out_delim_size, bit_descriptors);
    else if (type == "ordinal")
        gen_ordinal(values, offset, start_bit, out_delim_size, bit_descriptors);
    else if (type == "tree")
        gen_tree(values, offset, start_bit, out_delim_size, bit_descriptors);
    else
        gen_binary(values, offset, start_bit, out_delim_size);

//    printout();
    /*for (size_t i=0; i<bit_descriptors.size(); i++) std::cout << bit_descriptors[i] << " ";
    std::cout << "\r\n";*/

    setOuter(0, v_none);
//    printLatestAttr();
    return 0;
}


std::string Attribute::decToBin(unsigned int v, size_t minl)
{
    std::string res;
    while (v>0)
    {
        res.insert(0, std::to_string(v%2));
        v/=2;
    }
    for (size_t i=res.length(); i<minl; i++) res.insert(0,"0");
    if ("" == res) return ("0");
    return res;
}

int Attribute::setOffsets(size_t start, size_t delim)
{
    first_bit=start;
    last_bit=first_bit+encLen()+delim-1;
    return 0;
}

int Attribute::gen_binary(std::vector<std::string> values, size_t offset, size_t start_bit, size_t out_delim_size)
{
    unsigned int l=decToBin(values.size()-offset, 0).length();//bits needed, 1 value reserved for none
    //for (size_t i=offset; i<values.size(); i++) std::clog << values[i] <<"\r\n";
    //std::clog << l <<"\r\n" << "\r\n";
    m_enctable.clear();
    m_enctable.push_back(std::pair<std::string, std::string>("NONE", decToBin(0, l)));
    for (unsigned int i=offset; i<values.size(); i++)
    {
       m_enctable.push_back(std::pair<std::string, std::string>(values[i], decToBin(i-offset+1, l)/*leading zeros pushed inside*/));
    }
    setOffsets(start_bit, out_delim_size);
    return 0;
}

int Attribute::gen_selective(std::vector<std::string> values, size_t offset, size_t start_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors)
{
    size_t l=values.size()-offset;//bits needed, 1 value reserved for none
    //for (size_t i=offset; i<values.size(); i++) std::clog << values[i] <<"\r\n";
    //std::clog << l <<"\r\n" << "\r\n";
    m_enctable.clear();
    m_enctable.push_back(std::pair<std::string, std::string>("NONE", decToBin(0, l)));
    unsigned int curval=1;
    for (size_t i=offset; i<values.size(); i++)
    {
       m_enctable.push_back(std::pair<std::string, std::string>(values[i], decToBin(curval, l)/*leading zeros pushed inside*/));
       bit_descriptors[start_bit+l-1-i+offset]=name+"_"+values[i];//looks shitty, doesnt it?  hate that bitset numbering ideology!
       //std::clog << start_bit+l-1-i+offset << " " << values[i] <<"\r\n";
       curval*=2;
    }

    setOffsets(start_bit, out_delim_size);
    return 0;
}

int Attribute::gen_ordinal(std::vector<std::string> values, size_t offset, size_t start_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors)
{
    size_t l=values.size()-offset;//bits needed
    //for (size_t i=offset; i<values.size(); i++) std::clog << values[i] <<"\r\n";
    //std::clog << l <<"\r\n" << "\r\n";
    m_enctable.clear();
    m_enctable.push_back(std::pair<std::string, std::string>("NONE", decToBin(0, l)));
    unsigned int curval=1;
    for (size_t i=offset; i<values.size(); i++)
    {
       m_enctable.push_back(std::pair<std::string, std::string>(values[i], decToBin(curval, l)/*leading zeros pushed inside*/));
       curval=curval*2+1;
       bit_descriptors[start_bit+l-1-i+offset]=name+"_"+values[i];
    }

    setOffsets(start_bit, out_delim_size);

    return 0;
}

int Attribute::gen_tree(std::vector<std::string> unfilt_values, size_t offset, size_t start_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors)
{
    std::vector<std::string> values(unfilt_values.begin() + offset, unfilt_values.end());//only here. Not for other types. Maybe will dispose of this later, being less nervous


    std::vector<treenode*> nodes;
    treenode * vroot=new treenode("");
    nodes.push_back(vroot);
    for (size_t i=0; i< values.size(); i++)
    {
        boost::smatch m_lt, m_gt;
        if (boost::regex_search(values[i], m_lt, boost::regex("([^<>\n ]+) ?<? ?([^<>\n ]+)?")) or boost::regex_search(values[i], m_gt, boost::regex("([^<>\n ]+) ?>? ?([^<>\n ]+)?")))
        {
            std::string obj, parent;
            if (m_lt.empty())
            {
                obj=m_gt[2];
                parent=m_gt[1];
            }
            else
            {
                obj=m_lt[1];
                parent=m_lt[2];
            }
            //std::clog << "'" << obj << "' descendant of '" << parent << "'" << "\r\n";
            if (obj == "f")
                std::cout << "" ;
            //look whether already created as a parent
            treenode * p_treenode=vroot->findInDescendants(obj);
            if (p_treenode == NULL)
            {
                p_treenode=new treenode(obj);
                nodes.push_back(p_treenode);
            }
            else
            {
                //disconnect from previous parent
                vroot->orphanize(p_treenode);
            }
            treenode * p_parent;
            if ("" == parent) p_parent=vroot;
            else
            {
                p_parent=vroot->findInDescendants(parent);
                if (p_parent==NULL)
                {//parent not found: either not created yet or typo
                    p_parent=vroot->newChild(parent);
                    nodes.push_back(p_parent);
                }
                //else p_parent points to what we need already
            }
            p_parent->addChild(p_treenode);
        }
        else
        {
            std::cerr << "Error parsing tree-descripting string: " << values[i] << "\r\n" << "Whole attribute treated as selective." << "\r\n";
            gen_selective(unfilt_values, offset, start_bit, out_delim_size, bit_descriptors);
            return 1;
        }
    }
    std::vector<std::string> bit_descriptors_tmp(nodes.size()-1);
    vroot->generateEncodings(nodes.size()-1, bit_descriptors_tmp);//minus vroot - all zeros
    for (size_t i=0; i<bit_descriptors_tmp.size(); i++)
    {
        bit_descriptors[start_bit+bit_descriptors_tmp.size()-1-i]=name+"_"+bit_descriptors_tmp[i];
    }
    //m_bit_attrs=vroot->getNames();
    /*for (size_t i=0; i<m_bit_attrs.size(); i++)
    {
        std::clog << "[" << m_bit_attrs[i] << "] ";
    }
    std::clog << "\r\n";*/
    /*std::clog << vroot->toString() << "\r\n";
    for (size_t i=1; i<nodes.size(); i++)
    {
        std::clog << nodes[i]->text() << " -> " << nodes[i]->enc_str() << "\r\n";
    }*/
    m_enctable.clear();
    push_back("NONE", decToBin(0, nodes.size()-1));
    for (size_t i=1; i<nodes.size(); i++)
    {
       push_back(nodes[i]->text(), nodes[i]->enc_str());
       delete nodes[i];
    }
    setOffsets(start_bit, out_delim_size);
    //printout();
    return 0;
}

std::string Attribute::convert(std::string data, Direction direction)
{
    for (size_t j=0; j<m_enctable.size(); j++)
    {
        if ((direction==Direction::encoding?m_enctable[j].first:m_enctable[j].second) == data)
        {
            return (direction==Direction::decoding?m_enctable[j].first:m_enctable[j].second);
        }
    }
    return "";
}

bool Attribute::hasHumanValue(std::string v)
{
    if (m_enctable.size()<0)
    {
        std::cerr << "No encoding yet" << "\r\n";
        return false;
    }
    for (size_t i=0; i<m_enctable.size(); i++)
    {
        //std::clog << m_enctable[i].first << " =?= " << v << std::endl;
        if (m_enctable[i].first==v) return true;
    }
    return false;
}
