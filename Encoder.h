#ifndef ENCODER_H
#define ENCODER_H
#include <fstream>
#include <sstream>
#include "ObjDescription.h"
#include <tuple>
#include "EncodingLevel.h"
//#include "main.h"
#include <vector>

#include "text_const.h"

class Encoder
{
public:
//    Encoder(std::fstream &enc);
    Encoder(std::string fname_enc, std::string dummy_input_fname="");
    ~Encoder();
    bool initialized() {return m_attrs.size()>0;};
    int encodeAndSave(std::string desc, ObjDescription * p_input, size_t level);
    int printHumanInput(ObjDescription * p_input, size_t level, bool conv=true);
    void supposeIntraattributeDependencies();
    size_t attributes_qtty(size_t level) {if (level<0 or m_attrs.size()<level+1) {std::cerr << "No such attribute level" << "\r\n"; return 0;} return m_attrs[level]->size();};
    size_t bitAttributesQtty() {return m_attrs[BIT_COVERAGE]->size();}
    int attrFirstBitIndex(size_t level, size_t attr) {if (level<0 or attr<0 or m_attrs.size()<level+1) {std::cerr << "No such attribute level" << "\r\n"; return 0;} if (m_attrs[level]->size()<attr+1) {std::cerr << "No such attribute" << "\r\n"; return 0;} return m_attrs[level]->attrFirstBitIndex(attr);};
    int attrLastBitIndex(size_t level, size_t attr) {if (level<0 or attr<0 or m_attrs.size()<level+1) {std::cerr << "No such attribute level" << "\r\n"; return 0;} if (m_attrs[level]->size()<attr+1) {std::cerr << "No such attribute" << "\r\n"; return 0;} return m_attrs[level]->attrFirstBitIndex(attr);};
    std::string attrName(size_t level, size_t attr) {if (level<0 or attr<0 or m_attrs.size()<level+1) {std::cerr << "No such attribute level" << "\r\n"; return "ERROR";} if (m_attrs[level]->size()<attr+1) {std::cerr << "No such attribute" << "\r\n"; return "ERROR";} return m_attrs[level]->attrName(attr);};
    std::string bitAttrName(size_t bit) {if (bit>=m_attrs[BIT_COVERAGE]->size()) {std::cerr << " Attribute name for bit " << bit << " undefined" << "\r\n"; return "UNNAMED";} return attrName(BIT_COVERAGE, bit);};
//    Attribute * findAttributeAndLevel(std::string name, size_t &level);
 //   Attribute * findBitAttribute(size_t bit) {return m_attrs[BIT_COVERAGE]->findAttribute(bit,bit);};
  //  Attribute * findAttribute(std::string name, std::string posval="");
    size_t getTargetIndeces(std::vector<size_t> &res) {return m_attrs[BIT_COVERAGE]->getTargetIndeces(res);};

    size_t supposeEncLength() {size_t res= (m_attrs[BIT_COVERAGE]->size()>0?m_attrs[BIT_COVERAGE]->size():0); return res;};

    void getEncoding(std::fstream& enc_dirty, size_t level=0);
    int printEncoding(size_t level=0, std::fstream *fs=NULL)
    {
        if (m_attrs.size()<=level)
        {
            std::cerr << "Cannot print out encoding of layer " << level << ": no such layer\r" << std::endl;
            return 1;
        }
        std::cout << supposeEncLength() << " bits long" << std::endl;
        for (size_t i=0; i<m_attrs[level]->size(); i++) m_attrs[level]->printout(i, fs);
        return 0;
    }
    std::string howToEncode(std::string attr, std::string human_value, size_t &start);
    bool checkValues(ObjDescription * okay, std::string attr_n, ObjDescription * cand, size_t (&results)[4]);
    std::string human(std::string code) {return decode(code, 1);};
    std::string generateHumanDummy();
protected:
private:
    enum Direction
    {
        encoding,
        decoding
    };
    Encoder();
    Encoder(const Encoder&);//achtung! pointers!
    Encoder operator=(const Encoder&);
    //ObjDescription * p_input;
    std::string val_delim;
    std::string out_val_delim;

    //std::vector<size_t> m_attr_offset;
    std::vector<EncodingLevel*> m_attrs;//zero element is bit-by-bit coverage

    static const size_t BIT_COVERAGE=0;
    int parseAttribute(std::vector<std::string> data, size_t start_line_num, size_t level, std::vector<std::string> &bit_descriptors);
    //int genOneAttrReplacementTable(std::string orig, std::string type, std::string none, std::vector<std::string> data, unsigned int offset);
    std::string decode(std::string desc, size_t level);
    std::string encode(std::string desc, size_t level);
    std::string convert(std::string data, Direction, size_t level);

    void printLatestAttr();
    void printAttr(size_t level, size_t attr);
    void printLatestAttr(size_t level);
    void printBitEncoding() {m_attrs[BIT_COVERAGE]->printout();}

    std::string encFnameStrippedComments(std::fstream &enc_dirty);
    //Attribute * findAttribute(std::string name, size_t level, std::string posval="");
    bool findAttribute(std::string name, std::string posval, size_t &level, size_t &attr);
};

#endif // ENCODER_H
