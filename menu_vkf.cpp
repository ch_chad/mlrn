#include "menu_base.h"
int pre_vkf_demo_input_cut(std::vector<std::string>) {std::cerr << "pre_vkf_demo_input_cut: not implemented" << std::endl; return 1;}
int vkf_demo_vkf(std::vector<std::string>){env->VKFit("input", "predict", "result"); return 0;}
int vkf_demo_check(std::vector<std::string>)
{
    env->eraseSet("file");
    std::fstream data("result");
    if (!data or data.eof()) std::cerr << "No result file" << "\r\n";
    else
        {env->addDescriptionsFromMachineFile(data, "file");}
    data.close();
    env->checkValues("predict_check", "edible", "file");
    env->printStatistics();
    return 0;
}
int pre_vkf_demo_enc (std::vector<std::string>) {std::cerr << "pre_vkf_demo_enc: not implemented" << std::endl; return 1;}
int vkf_cube (std::vector<std::string> arg)
{
    size_t sz=(arg.size()==0?32:stoi(arg[0]));
    if (sz<=10) {std::clog << "Considering current settings VKF cannot reliably work with learn datasets smaller than 11. Aborting." << std::endl; return 1;}
    std::fstream cubefs("cube_enc.txt", std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    if (!cubefs) {std::cerr << "Error creating cube encoding file" << std::endl;return 1;}
    cubefs << ">values delimiter:[,]\r\n>attributes quantity: " << sz+1 << "\r\n----\r\n";
    cubefs << ">name:target\r\n>type:selective\r\n>target\r\n>none: *\r\n>values:\r\n0\r\n1\r\n----\r\n";
    for (size_t i=0; i<sz; i++)
    {
        cubefs << ">name:" << i << "\r\n>type:selective\r\n>values:\r\n1\r\n----\r\n";
    }
    cubefs.close();

    cubefs.open("cube.data", std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    for (size_t i=0; i<sz; i++)
    {
        cubefs << "10";
        for (size_t j=0; j<i; j++) cubefs << "1";
        cubefs << "0";
        for (size_t j=i+1; j<sz; j++) cubefs << "1";
        cubefs << "\r\n";
    }
    cubefs.close();

    cubefs.open("cube.data");
    read_encoding({"cube_enc.txt"});
//    read_data({"cube.data", "input"});
    env->addDescriptionsFromMachineFile(cubefs, "input");
    DataPart param;
    param.prob=0.5;
    param.name="predict";
    param.min_qtty=1;
    param.max_qtty=1;
    param.clone=false;
    param.target={0,1};
    param.create_check=true;
    env->cutToSets({param});

    all_sets_to_files_encoded({});

    vkf_demo_vkf({});

    return 0;
}

int vkf_demo_input(std::vector<std::string> arg)
{
    int a=read_encoding({"data\\mushrooms/enc.txt"});
    int b=read_data({"data/mushrooms/agaricus-lepiota.short", "input"});
    return std::max(a, b);
}
int pre_vkf_demo_input(std::vector<std::string> arg){std::cerr << "pre_vkf_input: Not implemented yet.\r" << std::endl; return 1;}
