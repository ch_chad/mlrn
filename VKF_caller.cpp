#include "VKF_caller.h"
#include <fstream>
VKF_caller::VKF_caller()
{
    //ctor
}

VKF_caller::~VKF_caller()
{
    //dtor
}

void VKF_caller::proceed (ObjDescription* in, ObjDescription* out)
{
    std::cout << "VKFing objdescriptions not implemented yet, sorry. Use files." << "\r\n";
}
void VKF_caller::proceed (std::string filein, std::string filepredict, std::string fileres)
{
    std::fstream fs(filein);
    if (!fs)
    {
        std::cerr << "Input file not found, aborting analysis\r" << std::endl; return;
    }
    size_t lines=0;
    std::string tmp;
    while (getline(fs,tmp)) lines++;
    if (lines<=10)
    {
        std::cerr << "Input file size is not sufficient: 11 is minimum for VKF\r" << std::endl; return;
    }
    std::string str="C:\\vkf\\VKF\\VKF.exe -l C:\\MachineLearning\\"+filein+" -p C:\\MachineLearning\\"+filepredict+" -r C:\\MachineLearning\\"+fileres+" -t 1 --compute=10 -b 1";
    int res=system(str.c_str());
    #if OUTPUT_LEVEL >= ULTRA_OUTPUT
        std::cout << "RESULT:\n{\n"<<res<<"\n}\n";
    #endif
}
