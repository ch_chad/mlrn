#ifndef DataPart_h
#define DataPart_h
#include <string>
#include <stdlib.h>
#include <vector>
#include <limits>
class DataPart
{
    public://i dont care, it could be a struct, but i needed default values assignment that seemed buggy in struct version
        std::string name="input";
        float prob=1;
        size_t min_qtty=0;
        size_t max_qtty=std::numeric_limits<size_t>::max();
        float min_share=0;
        float max_share=1;
        bool positive_only=false;
        bool negative_only=false;
        std::vector<size_t> target={};
        bool clone=false;
        bool create_check=false;
};
#endif
