#include <iostream>
#include "Analyzer_Timer.h"

void Analyzer_Timer::prepare()
{
    std::cout << "Dummy analyzer ready" << "\r\n";
}
void Analyzer_Timer::stop()
{
    std::cout << "Dummy analyzer stopped" << "\r\n";
}
std::string Analyzer_Timer::sumup(ObjDescription* result, ObjDescription* desired)
{
    std::cout << "Dummy analysis results satisfactory" << "\r\n";
    return "statistics okay";
}
