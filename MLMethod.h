#ifndef MLMETHOD_H
#define MLMETHOD_H
#include "ObjDescription_str.h"

class MLMethod
{
    public:
        virtual ~MLMethod();
        virtual void proceed (ObjDescription* in, ObjDescription* out)=0;
        virtual void proceed (std::string filein, std::string filepredict, std::string fileres)=0;
        virtual bool isProbabilistic()=0;
    protected:
        MLMethod();
    private:
};

#endif // MLMETHOD_H
