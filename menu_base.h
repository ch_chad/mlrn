#ifndef menu_base_h
#define menu_base_h
#include "main.h"

int interactive_usage_help(std::vector<std::string>);
int read_encoding(std::vector<std::string> fname);
int read_data(std::vector<std::string> arg);
bool checkEnvInit();
int print_human(std::vector<std::string> arg);
int print_machine(std::vector<std::string> arg);
int list_datasets(std::vector<std::string>);
int separate_set_auto(std::vector<std::string>);
int separate_set(std::vector<std::string> arg);
int pre_enc_data_desired (std::vector<std::string> arg);
int set_to_file(std::vector<std::string> arg);
int show_encoding(std::vector<std::string> arg);
int save_encoding(std::vector<std::string> arg);
int pre_enc(std::vector<std::string> arg);
int all_sets_to_files_encoded(std::vector<std::string> arg);
int all_sets_to_files_decoded(std::vector<std::string> arg);
int cut_by_file(std::vector<std::string> arg);
int reverse_poisonousness(std::vector<std::string>);
int nothing(std::vector<std::string> arg);
int clear_data(std::vector<std::string>);
int check(std::vector<std::string> args);
int setsize(std::vector<std::string> args);
int del_set(std::vector<std::string> arg);
#endif
