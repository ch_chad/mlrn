#ifndef ENCODINGLEVEL_H
#define ENCODINGLEVEL_H
#include "Attribute.h"
#include <vector>
class EncodingLevel
{
    public:
        EncodingLevel();
        ~EncodingLevel();
        EncodingLevel(const EncodingLevel& other);
        size_t size(){return m_attrs.size();};
        int attrFirstBitIndex(size_t attr) {if (attr<m_attrs.size()) return m_attrs[attr]->firstBit(); std::cerr << "No attribute #" << attr << " on this level, no first bit\r" << std::endl; return -1;};
        int attrLastBitIndex(size_t attr) {if (attr<m_attrs.size()) return m_attrs[attr]->lastBit(); std::cerr << "No attribute #" << attr << " on this level, no last bit\r" << std::endl; return -1;};
        std::string attrName(size_t attr) {if (attr<m_attrs.size()) return m_attrs[attr]->attrname(); std::cerr << "No attribute #" << attr << " on this level, no name\r" << std::endl; return "";};
        void printout(size_t attr, std::fstream *fs=NULL) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, cannot printout\r" <<std::endl; return;} m_attrs[attr]->printout(fs);}
        void printout(std::fstream *fs=NULL) {for (size_t i=0; i<m_attrs.size(); i++) printout(i, fs);};
        void push_back(Attribute* na) {m_attrs.push_back(na);};
        void clear() {m_attrs.clear();};
        void allFirstAndLastBitsAsOwnIndeces() {for (size_t i=0; i<m_attrs.size(); i++) {m_attrs[i]->firstBit(i); m_attrs[i]->lastBit(i); }};
        void attrname(size_t attr, std::string nname);
        bool isTarget(size_t attr) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, it cannot be target\r" <<std::endl; return false;} return m_attrs[attr]->isTarget();};
        std::string none(size_t attr) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, no none value\r" <<std::endl; return "";} return m_attrs[attr]->none();};
        void none(size_t attr, std::string nnone) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, cannot set none value\r" <<std::endl; return;} return m_attrs[attr]->none(nnone);};
        void updateBitDescriptors(std::vector<std::string> bit_descriptors);
        void copyTargetMarksForBitDescriptors(EncodingLevel * orig);
        size_t encLen() {size_t res=0; for (size_t i=0; i<size(); i++) res+=m_attrs[i]->encLen(); return res;};
        std::string decode(size_t attr, std::string value) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, cannot decode\r" <<std::endl; return "";} return m_attrs[attr]->decode(value);};
        std::string encode(size_t attr, std::string value) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, cannot encode\r" <<std::endl; return "";} return m_attrs[attr]->encode(value);};
//        std::string decode(std::string desc) {return "";};
//        std::string encode(std::string desc) {return "";};
        void push_back_dummy(std::string name) {std::clog << m_attrs.size() << std::endl; };
        int push_back_copy(std::string orig, bool target, std::string v_none, std::vector<std::string> &bit_descriptors);
        int push_back_new(std::string name, std::string type, std::string v_none, std::vector<std::string> data, size_t offset, size_t out_val_delim_length, std::vector<std::string> &bit_descriptors, bool target);

        size_t getTargetIndeces(std::vector<size_t> &res);

        std::string howToEncode(std::string attr, std::string human_value, size_t &start);
        bool findAttribute(std::string name, std::string posval, size_t &attr);
    protected:
    private:
        Attribute * findAttribute(size_t f_bit, size_t l_bit);
        std::vector<Attribute *> m_attrs;
        Attribute * findAttribute(std::string name, size_t level, std::string posval="");
        void copyBitDescriptors(Attribute * aorig, Attribute * nattr, std::vector<std::string> &bit_descriptors);
        void setTarget(size_t attr) {if (attr>m_attrs.size()-1) {std::cerr << "No attribute #" << attr << " on this level, it cannot be made target\r" <<std::endl; return;} m_attrs[attr]->setTarget();}
        Attribute * findAttribute(std::string name, std::string posval="");

};

#endif // ENCODINGLEVEL_H


