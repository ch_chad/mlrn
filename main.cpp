#include <iostream>
#include <fstream>
//#include <getopt.h>


#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
//#include <boost/algorithm/string.hpp>

#include "main.h"
#include "Action.h"
#include "output_levels.h"
#include "MLEnv.h"
#include "MLMethod_ID3.h"
#include "menu_base.h"
#include "menu_vkf.h"
//using namespace std;

const char* program_name;
std::vector<Action> menu;
MLEnv * env=NULL;

//std::string def_enc_fname="encoding_mushrooms.txt", def_data_fname="agaricus_lepiota.short";

/*std::string ndata;
std::string nenc;
std::string npredict;
std::string nlearn;*/



//void print_usage_info(const int exit_code);

void test_cutter();



int interactive_mode();
//http://stackoverflow.com/questions/236129/split-a-string-in-c

int construct_menu()
{
    boost::smatch m;

    /*

 .----------------.  .----------------.
| .--------------. || .--------------. |
| | _____  _____ | || |  _________   | |
| ||_   _||_   _|| || | |_   ___  |  | |
| |  | | /\ | |  | || |   | |_  \_|  | |
| |  | |/  \| |  | || |   |  _|  _   | |
| |  |   /\   |  | || |  _| |___/ |  | |
| |  |__/  \__|  | || | |_________|  | |
| |              | || |              | |
| '--------------' || '--------------' |
 '----------------'  '----------------'
 .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. || .--------------. || .--------------. |
| |   _____      | || |     ____     | || | ____   ____  | || |  _________   | |
| |  |_   _|     | || |   .'    `.   | || ||_  _| |_  _| | || | |_   ___  |  | |
| |    | |       | || |  /  .--.  \  | || |  \ \   / /   | || |   | |_  \_|  | |
| |    | |   _   | || |  | |    | |  | || |   \ \ / /    | || |   |  _|  _   | |
| |   _| |__/ |  | || |  \  `--'  /  | || |    \ ' /     | || |  _| |___/ |  | |
| |  |________|  | || |   `.____.'   | || |     \_/      | || | |_________|  | |
| |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'
 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |  _______     | || |  _________   | || |    ______    | || | _____  _____ | || |   _____      | || |      __      | || |  _______     | |
| | |_   __ \    | || | |_   ___  |  | || |  .' ___  |   | || ||_   _||_   _|| || |  |_   _|     | || |     /  \     | || | |_   __ \    | |
| |   | |__) |   | || |   | |_  \_|  | || | / .'   \_|   | || |  | |    | |  | || |    | |       | || |    / /\ \    | || |   | |__) |   | |
| |   |  __ /    | || |   |  _|  _   | || | | |    ____  | || |  | '    ' |  | || |    | |   _   | || |   / ____ \   | || |   |  __ /    | |
| |  _| |  \ \_  | || |  _| |___/ |  | || | \ `.___]  _| | || |   \ `--' /   | || |   _| |__/ |  | || | _/ /    \ \_ | || |  _| |  \ \_  | |
| | |____| |___| | || | |_________|  | || |  `._____.'   | || |    `.__.'    | || |  |________|  | || ||____|  |____|| || | |____| |___| | |
| |              | || |              | || |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .-----------------. .----------------.
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |  _________   | || |  ____  ____  | || |   ______     | || |  _______     | || |  _________   | || |    _______   | || |    _______   | || |     _____    | || |     ____     | || | ____  _____  | || |    _______   | |
| | |_   ___  |  | || | |_  _||_  _| | || |  |_   __ \   | || | |_   __ \    | || | |_   ___  |  | || |   /  ___  |  | || |   /  ___  |  | || |    |_   _|   | || |   .'    `.   | || ||_   \|_   _| | || |   /  ___  |  | |
| |   | |_  \_|  | || |   \ \  / /   | || |    | |__) |  | || |   | |__) |   | || |   | |_  \_|  | || |  |  (__ \_|  | || |  |  (__ \_|  | || |      | |     | || |  /  .--.  \  | || |  |   \ | |   | || |  |  (__ \_|  | |
| |   |  _|  _   | || |    > `' <    | || |    |  ___/   | || |   |  __ /    | || |   |  _|  _   | || |   '.___`-.   | || |   '.___`-.   | || |      | |     | || |  | |    | |  | || |  | |\ \| |   | || |   '.___`-.   | |
| |  _| |___/ |  | || |  _/ /'`\ \_  | || |   _| |_      | || |  _| |  \ \_  | || |  _| |___/ |  | || |  |`\____) |  | || |  |`\____) |  | || |     _| |_    | || |  \  `--'  /  | || | _| |_\   |_  | || |  |`\____) |  | |
| | |_________|  | || | |____||____| | || |  |_____|     | || | |____| |___| | || | |_________|  | || |  |_______.'  | || |  |_______.'  | || |    |_____|   | || |   `.____.'   | || ||_____|\____| | || |  |_______.'  | |
| |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
 .----------------.
| .--------------. |
| |              | |
| |      _       | |
| |     | |      | |
| |     | |      | |
| |     | |      | |
| |     |_|      | |
| |     (_)      | |
| '--------------' |
 '----------------'


    */
    std::string re_unmatcheable="\b\B";
    std::string re_fname="(([^ ]+)|(\"([^\"]*)\")|('([^']*)'))";
    std::string re_dataset="([^ ]+)";
    std::string re_file_or_set="("+re_fname+"|"+re_dataset+")";
    matcher cmd_ihelp(          "(--inter_help)|(-i)",                                                                          "--inter_help or -i",                                               {0});
    matcher i_ihelp(            "h(elp)?",                                                                                      "help",                                                             {0});
    matcher nomatch(            re_unmatcheable,                                                                                "Not available",                                                    {0});
    matcher i_getenc(           "(((get)|(read)) )?enc(oding)? from ?"+re_fname,                                                 "get encoding from {filename}",                                     {6});
    matcher i_gethuman(         "(((get)|(read)) )?((human)|((human )?data)) from ?"+re_fname+"( ?(to (dataset )?)?"+re_dataset+")?",   "get human data from {filename} to dataset {datasetname}",   {9, 18});
    matcher i_showhuman(        "((show )|(print ))?((human)|((human )?data)) ?"+re_dataset,                                    "show human data {datasetname}",                                    {8});
    matcher i_showmachine(      "((show )|(print ))?machine (data)? ?"+re_dataset,                                              "show machine data {datasetname}",                                  {5});
    matcher i_showsets(         "((show )|(print ))?(data)?sets",                                                               "show datasets",                                                    {0});
    matcher i_autocut(          "set "+re_dataset+" autocut",                                                                   "set {datasetname} autocut",                                        {1});
    matcher i_showenc(          "((show )|(print ))?enc(oding)? ?([0-9]+)?",                                                    "show encoding {integer_level}",                                    {5});
    matcher i_setsize(          "(set ?)?size "+re_dataset,                                                                     "set size {datasetname}",                                           {2});
    matcher cmd_vkf_in(         "--VKF_demo_input",                                                                             "--VKF_demo_input",                                                 {0});
    matcher i_vkf_in(           "VKF_demo input",                                                                               "VKF_demo input",                                                   {0});
    matcher cmd_vkf_enc_print(  "--VKF_demo_encoded(_printout)?",                                                               "--VKF_demo_encoded_printout",                                      {0});
    matcher i_vkf_enc_print(    "VKF_demo encoded( printout)?",                                                                 "VKF_demo encoded printout",                                        {0});
    matcher cmd_vkf_dec_print(  "--VKF_demo_decoded(_printout)?",                                                               "--VKF_demo_decoded_printout",                                      {0});
    matcher i_vkf_dec_print(    "VKF_demo decoded( printout)?",                                                                 "VKF_demo decoded printout",                                        {0});
    matcher cmd_vkf_cut_file(   "--VKF_demo _cut ?=? ?"+re_fname,                                                               "--VKF_demo_cut = {filename.txt}",                                  {1});
    matcher i_vkf_cut_file(     "VKF_demo cut ?=? ?"+re_fname,                                                                  "VKF_demo cut = {filename.txt}",                                    {1});
    matcher cmd_vkf_to_files(   "--VKF_demo_(sets_)?to_files",                                                                  "--VKF_demo_sets_to_files",                                              {0});
    matcher i_vkf_to_files(     "VKF_demo (sets )?to files",                                                                    "VKF_demo sets to files",                                           {0});
    matcher cmd_vkf_to_files_h( "--VKF_demo_(sets_)?to_files_human",                                                            "--VKF_demo_sets_to_files_human",                                        {0});
    matcher i_vkf_to_files_h(   "VKF_demo to files human",                                                                      "VKF_demo sets to files human",                                          {0});
    matcher cmd_vkf_swap(       "--VKF_demo_(reverse_poisonousness)|(swap_p_e)((_in(_(data)?set)?)? "+re_fname+")?",            "--VKF_demo_reverse_poisonousness_in_dataset {datasetname}",        {7});
    matcher i_vkf_swap(         "VKF_demo ((reverse poisonousness)|(swap p e))(( in( (data)?set)?)? "+re_fname+")?",            "VKF_demo reverse poisonousness in dataset {datasetname}",          {7});
    matcher cmd_vkf_vkf(        "--VKF_demo_start_VKF",                                                                         "--VKF_demo_start_VKF",                                             {0});
    matcher i_vkf_vkf(          "VKF_demo (start )?VKF",                                                                        "VKF_demo start VKF",                                               {0});
    matcher cmd_vkf_check(      "--VKF_demo_check",                                                                             "--VKF_demo_check",                                                 {0});
    matcher i_vkf_check(        "VKF_demo check",                                                                               "VKF_demo check",                                                   {0});
    matcher i_check(            "check "+re_dataset+" ([^ ]+) "+re_dataset,                                                     "check {candidatedatasetname} {attributename} {okaydatasetname}",   {1, 2, 3});
    matcher i_vkf_purge(        "VKF_demo purge",                                                                               "VKF_demo purge",                                                   {0});
    matcher cmd_vkf_cube(       "--VKF_demo_(generate_)?cube ([0-9]+)",                                                         "--VKF_demo_generate_cube {integer}",                               {2});
    matcher i_vkf_cube(         "VKF_demo (generate )?cube ([0-9]+)",                                                           "VKF_demo generate cube {integer}",                                 {2});
    matcher i_del_set(          "delete ((data)?set )?"+re_dataset,                                                             "delete dataset {setname}",                                         {3});
    /*                                                                                                                                                                                                  arguments from interactive              preliminary             preliminary
                            cmd regex                                                       arguments from cmd                  interactive mode regex                                                                                          for cmd                 params          main command                help*/
    menu.push_back(Action (cmd_ihelp,           i_ihelp,            *nothing,               {""},           *interactive_usage_help,    "Show list of commands for interactive mode"));
    menu.push_back(Action (nomatch,             i_getenc,           *nothing,               {""},           *read_encoding,             "Purge internal database and get encoding from a file"));
    menu.push_back(Action (nomatch,             i_gethuman,         *nothing,               {""},           *read_data,                 "Get data from a file"));
    menu.push_back(Action (nomatch,             i_showhuman,        *nothing,               {""},           *print_human,               "Print data in human form"));
    menu.push_back(Action (nomatch,             i_showmachine,      *nothing,               {""},           *print_machine,             "Print data in machine form"));
    menu.push_back(Action (nomatch,             i_showsets,         *nothing,               {""},           *list_datasets,             "Print dataset names"));
    menu.push_back(Action (nomatch,             i_autocut,          *nothing,               {""},           *separate_set_auto,         "Move part of input to a separate dataset (cutter options hard-coded)"));
    //menu.push_back(Action ("(--set)|(-s) ?=? ?"+re_dataset+" "+re_fname,                                                        "set "+re_dataset+" to file "+re_fname,                 *pre_enc_data_desired,  "",         *set_to_file,               "Save dataset to a file in machine format"));
    menu.push_back(Action (nomatch,             i_showenc,          *nothing,               {""},           *show_encoding,             "Show encoding. Level 0 - raw bit characteristics. Level 1 - contents of encoding file"));
    //menu.push_back(Action ("(--encoding)|(-e) "+re_fname,                                   {3},                                "save enc(oding)? "+re_fname,                                           {2},                                    *pre_enc,               {def_enc_fname},*save_encoding,             "Save encoding description to a file"));
    //menu.push_back(Action ("(--compare)|(-c) "+re_fname+" "+re_fname,                       "((cmp)|(compare) )?sets "+re_dataset+" "+re_dataset,   *pre_enc,               "",         *comp_sets,                 "Compare two datasets"));
    //menu.push_back(Action ("(--check)|(-k) "+re_file_or_set+" "+re_file_or_set,             "check "+re_file_or_set+" "+re_file_or_set,             *nothing,               "",         *desired_cmp_check,         "Check target values of dataset 1 with dataset 2"));
    //menu.push_back(Action (re_unmatcheable,                                                    "(clr)|(clear)  "+re_dataset,                           *nothing,               "",         *clear_set,                 "Delete all entries from a dataset"));
    //menu.push_back(Action (re_unmatcheable,                                                    "read (set )?"+re_dataset+" "+re_fname,                 *pre_enc,               "",         *read_set,                  "Read dataset from file"));
    menu.push_back(Action (cmd_vkf_in,          i_vkf_in,           *nothing,               {""},           *vkf_demo_input,            "Read encoding 'encoding_mushrooms.txt' and data 'agaricus_lepiota.data'"));
    menu.push_back(Action (cmd_vkf_enc_print,   i_vkf_enc_print,    *pre_vkf_demo_input,    {""},           *print_machine,             "Print data in machine form"));
    menu.push_back(Action (cmd_vkf_dec_print,   i_vkf_dec_print,    *pre_vkf_demo_input,    {""},           *print_human,               "Print data in human form"));
    menu.push_back(Action (cmd_vkf_cut_file,    i_vkf_cut_file,     *pre_vkf_demo_input,    {""},           *cut_by_file,               "Cut input dataset according to given parameter file"));
    menu.push_back(Action (cmd_vkf_to_files,    i_vkf_to_files,     *pre_vkf_demo_input_cut,{""},           *all_sets_to_files_encoded, "Send all datasets to separate files in machine form"));
    menu.push_back(Action (cmd_vkf_to_files_h,  i_vkf_to_files_h,   *pre_vkf_demo_input_cut,{""},           *all_sets_to_files_decoded, "Send all datasets to separate files in human form"));
    menu.push_back(Action (cmd_vkf_swap,        i_vkf_swap,         *pre_vkf_demo_input_cut,{""},           *reverse_poisonousness,     "Make all poisonous mushrooms edible and reverse... I don't remember what for :) Maybe negative causes search"));
    menu.push_back(Action (cmd_vkf_vkf,         i_vkf_vkf,          *nothing,               {""},           *vkf_demo_vkf,              "Start VKF for files 'input' and 'predict'"));
    menu.push_back(Action (cmd_vkf_check,       i_vkf_check,        *pre_vkf_demo_enc,      {""},           *vkf_demo_check,            "Compare values predicted by VKF to correct answers in dataset 'predict_check'"));
    menu.push_back(Action (nomatch,             i_check,            *nothing,               {""},           *check,                     "Compare value of an attribute (2) in given dataset (1) to correct answers that another dataset (3) consists solely of"));
    menu.push_back(Action (nomatch,             i_vkf_purge,        *nothing,               {""},           *clear_data,                "Delete all datasets"));
    menu.push_back(Action (cmd_vkf_cube,        i_vkf_cube,         *nothing,               {""},           *vkf_cube,                  "Generate an almost all-ones hypercube, cut it a bit and send it to files"));
    menu.push_back(Action (nomatch,             i_del_set,          *nothing,               {""},           *del_set,                   "Delete a dataset by name"));
    //menu.push_back(Action ("--D_datasets",                                                  "D_datasets",                                           *pre_enc_data,          "",         *d_datasets,                "Send description of all datasets to 'datasets.txt'"));
    //menu.push_back(Action ("--D_cutting_params",                                            "D_cutting_params",                                     *nothing,               "",         *d_cutting_params,          "Send description of all possible options used while cutting datasets to 'cut_params.txt'"));
    //menu.push_back(Action ("--D_cut ?=? ?"+re_fname,                                        "D_cut ?=? ?"+re_fname,                                 *pre_enc_data,          "",         *d_cut,                     "Cut input dataset according to options specified in given file and put resulting datasets to separate files 'setname.data' plus a 'setname.check' for every setname with target attributes cleared"));
    //menu.push_back(Action ("--D_methods",                                                   "D_methods",                                            *nothing,               "",         *d_methods,                 "Send description of all available machine learning methods to 'methods.txt'"));
    //menu.push_back(Action ("--D_process "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname,\
                                                        "D_process "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname+" "+re_fname,     *nothing,               "",         *d_process,                 "Start machine learning method specified in file 1 with file 2 as a training set, file 3 as a prediction set, file 4 as check set. Put prediction result to 'predicted.data' and statistics to 'statistics.txt'"));


    menu.push_back(Action (nomatch,             i_setsize,           *nothing,               {""},           *setsize,          "Show quantity of descriptions in given dataset"));
    return 0;
}
int cmd_mode(int argc, char* argv[]);

int main(int argc, char* argv[])
{
    //TODO: logging
    //http://stackoverflow.com/questions/2638654/redirect-c-stdclog-to-syslog-on-unix
    //http://stackoverflow.com/questions/533038/redirect-stdcout-to-a-custom-writer
    //http://stackoverflow.com/questions/4810516/c-redirecting-stdout

    program_name = argv[0];
//    defaults_file_read();
    construct_menu();

    if (argc==2 and (strcmp(argv[1],"-x")==0 or strcmp(argv[1],"--interactive")==0))
    {
        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Operating in interactive mode. h for help.\r" << std::endl;
        #endif // OUTPUT_LEVEL
        return interactive_mode();
    }
    else
    {
        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Operating in command-line mode. --help for help.\r" << std::endl;
        #endif // OUTPUT_LEVEL
        //std::cerr << "[" <<argv[1]<< "] Sorry, dude, nothing here yet :|\r" << std::endl;
        //return 0;//cmd_mode(argc, argv);
        return cmd_mode(argc, argv);
    }
}

int cmd_mode(int argc, char* argv[])
{
    std::stringstream s;
    for (int i=1; i<argc; i++) s << argv[i] << " ";
    std::string c_str=s.str();
    for (size_t i=0; i<menu.size(); i++)
        if (menu[i].match_cmd(c_str))
        {
            return menu[i].start_cmd();
        }
    std::cerr << c_str << " : Command not recognized\r" <<std::endl;
    return 1;
}

int interactive_mode()
{
    bool done=false;
    while (true)
    {
        done=false;
        std::string c_str;
        std::cout << "\n> ";
        getline(std::cin, c_str);
        if (c_str=="exit" or c_str=="quit" or c_str=="0" or c_str=="q") return 0;
        for (size_t i=0; i<menu.size(); i++)
            if (menu[i].match_interactive(c_str))
            {
                menu[i].start();
                done=true;
                break;
            }
        if (!done) std::cerr << c_str << " : Command not recognized\r" <<std::endl;
    }
/*      case 'y':
            //for DW
//           env->addDescriptionsFromHumanFile("input");
            //env->deleteAttribute("edible_p", "input")
            test_cutter(env);
            env->cloneSet("desired", "check");
            env->purgeTargets("desired");
            outf.close();
            outf.open(npredict);
            env->sendSetToFile("desired", outf);
            outf.close();
            learnf.close();
            learnf.open(nlearn);
            env->sendSetToFile("input", learnf);
            learnf.close();
            break;
        case 'u':
            env->eraseSet("file");
            data.close();
            data.open("output.data");
            if (!data or data.eof()) std::cerr << "No result file" << "\r\n";
            else
                {env->addDescriptionsFromMachineFile(data, "file");}
            data.close();
            env->checkValues("check", "edible", "file");
            env->printStatistics();
            break;
        case 'o':
            env->findDependenciesWithOuterPredictor();
            break;
        case 'p':
            env->estimateDataSizeForAccuracy();
            break;
    }*/
}

void dummyEncoding(std::string enc_fname)
{
    //std::clog << enc_fname <<std::endl;
    std::fstream dummy(enc_fname, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    dummy << ">values delimiter:[,]\r\n>attributes quantity: 1\r\n----\r\n>name: error\r\n>type: selective\r\n>target\r\n>values:\r\nyeah\r\nsure\r\n----";
    dummy.close();
}
