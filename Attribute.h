#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include "treenode.h"
#include <boost/regex.hpp>
#include <fstream>
class Attribute
{
public:
    Attribute();
    Attribute(std::string n):name(n),first_bit(0), last_bit(0) {};
    Attribute(Attribute * orig);
    size_t firstBit() {return first_bit;};//oh god, all this shit works only by prayers... I hate that dbs numbering and hate the idea of using dbs at all... RAWWWWRRRRRRRR!
    void firstBit(size_t b) {first_bit=b;};
    size_t lastBit() {return last_bit;};
    void lastBit(size_t b) {last_bit=b;};
    std::string attrname() {return name;};
    void attrname(std::string a) {name=a;};
    void printout(std::fstream *fs);
    void push_back(std::string from, std::string to) {m_enctable.push_back(std::pair<std::string, std::string> (from, to));};
    size_t encLen() {if (m_enctable.size()>1) { return ((m_enctable[0]).second).length();} /*else*/std::cerr << "No encoding table yet" << "\r\n"; return 0;}
    //void setNone(std::string f, std::string t) {if (m_enctable.size()>0) {m_enctable[0].first=f;m_enctable[0].second=t;} /*else*/std::cerr << "No encoding table yet" << "\r\n"; return;};
    std::string none() {if (m_enctable.size()>0) return m_enctable[0].first; std::cerr << "No encoding yet" << "\r\n"; return "";};
    void none(std::string v) {if (m_enctable.size()==0) {std::cerr << "Cannot set none value: length unknown yet" << "\r\n"; return;} m_enctable[0].first=v;};
    std::string outer(size_t val) {return m_enctable[val].first;};
    void setOuter(size_t val, std::string to) {m_enctable[val].first=to;};
    void setTarget(bool t){std::clog << "It will be target someday :)" << "\r\n";};
    int genReplacementTable(std::string type, std::string v_none, std::vector<std::string> values, size_t offset_values, size_t start_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors);
    int genReplacementTable(std::string v_none, size_t start_bit);//for copied only
    std::string encode(std::string data) {return convert(data, Direction::encoding);};
    std::string decode(std::string data) {return convert(data, Direction::decoding);};
    bool isTarget() {return target;};
    void setTarget() {/*std::clog << attrname() << " is now target" << std::endl; */target=true;};
    bool hasHumanValue(std::string v);
    bool addValue(std::string from, std::string to) {if (encode(from)!="" or decode(to)!="") {std::cerr << "Cannot use these values, they cause ambiguity\r" << std::endl; return false;} std::pair<std::string, std::string> t; t.first=from, t.second=to; m_enctable.push_back(t); return true;};
private:
    enum Direction
    {
        encoding,
        decoding
    };
    std::string convert(std::string data, Direction direction);
    Attribute(const Attribute& other);
    std::string name;
    bool target=false;
    size_t first_bit;
    size_t last_bit;
    std::vector<std::pair<std::string, std::string> > m_enctable;

    int gen_selective(std::vector<std::string> values, size_t offset, size_t prev_attr_last_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors);
    int gen_ordinal(std::vector<std::string> values, size_t offset, size_t prev_attr_last_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors);
    int gen_binary(std::vector<std::string> values, size_t offset, size_t prev_attr_last_bit, size_t out_delim_size);
    int gen_tree(std::vector<std::string> values, size_t offset, size_t prev_attr_last_bit, size_t out_delim_size, std::vector<std::string> &bit_descriptors);

    std::string decToBin(unsigned int v, size_t minl);
    int setOffsets(size_t prev, size_t delim);
};


#endif // ATTRIBUTE_H
