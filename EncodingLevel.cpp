#include "EncodingLevel.h"

EncodingLevel::EncodingLevel()
{

    //ctor
}

EncodingLevel::~EncodingLevel()
{
    //dtor
}

EncodingLevel::EncodingLevel(const EncodingLevel& other)
{
    //copy ctor
}

void EncodingLevel::updateBitDescriptors(std::vector<std::string> bit_descriptors)
{
    while (size()<bit_descriptors.size()) push_back(new Attribute());
    allFirstAndLastBitsAsOwnIndeces();
    for (size_t i=0; i<bit_descriptors.size(); i++)
    {
        attrname(bit_descriptors.size()-i-1, bit_descriptors[i]);
        m_attrs[i]->addValue("1", "1");//dummy; enclevel is selfdescripting anyway, but we need this for enclen calculations
    }
    //printout();
}

void EncodingLevel::copyTargetMarksForBitDescriptors(EncodingLevel * orig)
{
//    std::clog << orig->encLen() << std::endl;
    for (size_t i=0; i<orig->size(); i++)
    {
        if (orig->isTarget(i))
        {
            //if (orig->none(i)=="") m_attrs[level]->none(i, "NONE");
            for (size_t j=orig->attrFirstBitIndex(i); j<=orig->attrLastBitIndex(i); j++)
            {
                setTarget(orig->encLen()-j-1);
                //m_attrs[encLen()-j-1]->setNone("-");
                //std::cout << m_attrs[orig->encLen()-j-1]->attrname() << " is target" << "\r\n";
            }
        }
    }
}

/*Attribute * EncodingLevel::findAttribute(std::string name, size_t level, std::string posval)
{
    for (size_t j=0; j<size(); j++)
    {

    }
    return NULL;
}*/

bool EncodingLevel::findAttribute(std::string name, std::string posval, size_t &attr)
{
    Attribute * a=findAttribute(name, posval);
    if (a==NULL) return false;
    for (size_t i=0; i<m_attrs.size(); i++)
        if (m_attrs[i]==a) {attr=i; return true;}
    return false;
}

Attribute * EncodingLevel::findAttribute(std::string name, std::string posval)
{
    Attribute * r;
    for (size_t j=0; j<m_attrs.size(); j++)
    {
        //std::clog << attrName(j) << std::endl;
        if (attrName(j)==name)
            if (posval=="" or m_attrs[j]->hasHumanValue(posval)) return m_attrs[j];
    }
    return NULL;
}

Attribute * EncodingLevel::findAttribute(size_t f_bit, size_t l_bit)
{
        for (size_t j=0; j<m_attrs.size(); j++)
        {
            //std::clog << m_attrs[i][j]->attrname() << " (" << m_attrs[i][j]->firstBit() << "), ";
            if (m_attrs[j]->firstBit()==f_bit && m_attrs[j]->lastBit()==l_bit) {/*std::clog << "\r\n";*/ return m_attrs[j];}
        }
    return NULL;
}



void EncodingLevel::copyBitDescriptors(Attribute * aorig, Attribute * nattr, std::vector<std::string> &bit_descriptors)
{
    size_t offset=nattr->firstBit();
    while (bit_descriptors.size()<offset+nattr->encLen()) bit_descriptors.push_back("");
    for (size_t i=aorig->firstBit(); i<aorig->lastBit(); i++)
    {
        bit_descriptors[offset+i]=bit_descriptors[i];
    }
}

size_t EncodingLevel::getTargetIndeces(std::vector<size_t> &res)
{
    for (size_t i=0; i<size(); i++)
        if (isTarget(i))
            res.push_back(i);
    return res.size();
}

std::string EncodingLevel::howToEncode(std::string attr, std::string human_value, size_t &start)
{
    Attribute * res=findAttribute(attr, human_value);
    if (res==NULL) return "";
    start=encLen()-res->lastBit()-1;
    return res->encode(human_value);
}

int EncodingLevel::push_back_new(std::string name, std::string type, std::string v_none, std::vector<std::string> data, size_t offset, size_t out_val_delim_length, std::vector<std::string> &bit_descriptors, bool target)
{
    Attribute * nattr=new Attribute(name);
    m_attrs.push_back(nattr);
    size_t start_bit=(m_attrs.size()==1?0:m_attrs[m_attrs.size()-2]->lastBit()+1);
    int res=nattr->genReplacementTable(type, v_none, data, offset, start_bit, out_val_delim_length, bit_descriptors);
    if (target) nattr->setTarget();
    return 0;
}

int EncodingLevel::push_back_copy(std::string orig, bool target, std::string v_none, std::vector<std::string> &bit_descriptors)
{
    Attribute * aorig=findAttribute(orig);
    Attribute * nattr=new Attribute(aorig);
    m_attrs.push_back(nattr);
    size_t start_bit=(m_attrs.size()==1?0:m_attrs[m_attrs.size()-2]->lastBit()+1);
    int res= nattr->genReplacementTable(v_none, start_bit);
    copyBitDescriptors(aorig, nattr, bit_descriptors);
    if (target) nattr->setTarget();
    return 0;
}

void EncodingLevel::attrname(size_t attr, std::string nname)
{
    if (attr>m_attrs.size()-1)
    {
        std::cerr << "No attribute #" << attr << " on this level, cannot rename to " << nname << "\r" <<std::endl;
        return;
    }
    m_attrs[attr]->attrname(nname);
}
