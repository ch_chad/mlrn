#include "menu_base.h"
#include <vector>
#include <string>
#include <stdlib.h>
#include <iostream>

int interactive_usage_help(std::vector<std::string>) {for(size_t m=0; m<menu.size(); m++) std::cout<<menu[m].toString()<<std::endl<<std::endl; return 0;}
int read_encoding(std::vector<std::string> fname)
{
    if (env!=NULL) delete env;
    env=new MLEnv(fname[0]);
    if (!env->encoder_okay())
    {
        std::cerr << "Error reading encoding file: using a dummy\r" << std::endl;
        env->make_default_enc("dummy.txt");
        delete env;
        env=new MLEnv("dummy.txt");
    }
    return 0;
}
int read_data(std::vector<std::string> arg) {
//    for (size_t i=0; i<arg.size(); i++) std::clog << arg[i] <<std::endl;
    if (env==NULL) {std::cerr << "Environment not initialized yet" << std::endl; return 1;}
    std::string fname=arg[0], dataset=arg[1];
    //std::clog << fname << " " << dataset << std::endl;
    std::fstream f(fname);
    return env->addDescriptionsFromHumanFile(f, dataset);
}
bool checkEnvInit() {if(env==NULL) {std::cerr << "Environment not initialized yet" << std::endl; return false;}return true;}
int print_human(std::vector<std::string> arg)
{
    if (!checkEnvInit()) return 1;
    if (arg.size()==0) return env->printDataset("", true);
    return env->printDataset(arg[0], true);
}
int print_machine(std::vector<std::string> arg)
{
    if (!checkEnvInit()) return 1;
    if (arg.size()==0) return env->printDataset("", false);
    return env->printDataset(arg[0], false);
}
int list_datasets(std::vector<std::string>){if (!checkEnvInit()) return 1; return env->listDatasets();}

void test_cutter()
{
    std::vector <DataPart> params;
    params.push_back(DataPart());//TODO: no good actually. errors possible if the user doesnt know about this construction way
    params[0].name="desired";
    params[0].min_share=0.1;
    //params[0].min_qtty=3;
    //params[0].max_qtty=3;
    params[0].max_share=0.2;
    params[0].prob=0.1;
    params[0].clone=false;
    env->cutToSets(params);
}


int separate_set_auto(std::vector<std::string>) {if (!checkEnvInit()) return 1; test_cutter(); return 0;}
int separate_set(std::vector<std::string> arg/*set, cutting params*/) {std::cerr << "separate_set: Not implemented yet\r\n"; return 1;}
int pre_enc_data_desired (std::vector<std::string> arg) {/*return std::max(std::max(read_encoding(default_encoding_fname), read_data(default_data_fname)), separate_set("|desired|default_cut.txt")); return 1;*/return 0;/*else warning pops up*/}
int set_to_file(std::vector<std::string> arg) {std::cerr << "set_to_file: Not implemented yet. Plans: just like separate_set, but send to file named after set.data\r\n"; return 1;}
int show_encoding(std::vector<std::string> arg){if (env!=NULL) return env->printEncoding((arg.size()==0?1:(arg[0]==""?0:stoi(arg[0])))); else std::cerr << "Cannot show encoding: environment not initialized yet\r"<<std::endl; return 1;}
int save_encoding(std::vector<std::string> arg){if (env!=NULL) {std::fstream * fs=new std::fstream(arg[0]); int res=env->printEncoding(1, fs); delete fs; return res;} else std::cerr << "Cannot save encoding: environment not initialized yet\r"<<std::endl; return 1;}
int pre_enc(std::vector<std::string> arg) {return read_encoding(arg);}
int all_sets_to_files_encoded(std::vector<std::string> arg)
{
    return env->sendSetsToFiles();
}
int all_sets_to_files_decoded(std::vector<std::string> arg)
{
    return env->sendSetsToFiles(true);
}


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

DataPart parse_cut_param_file(std::fstream *fs)
{
    boost::smatch m;
    std::string buf;
    DataPart param;
    while (getline(*fs, buf))
    {
        if (boost::regex_search(buf, m, boost::regex("name ?= ?\"?([^ \"\n]+)\"?"))) param.name=m[1];
        if (boost::regex_search(buf, m, boost::regex("prob(ability)? ?= ?\"?([0-9\.]+)\"?"))) param.prob=stof(m[2]);
        if (boost::regex_search(buf, m, boost::regex("min(_qtty)? ?= ?\"?([0-9]+)\"?"))) param.min_qtty=stoi(m[2]);
        if (boost::regex_search(buf, m, boost::regex("min_share ?= ?\"?([0-9\.]+)\"?"))) param.min_share=stof(m[1]);
        if (boost::regex_search(buf, m, boost::regex("max(_qtty)? ?= ?\"?([0-9]+)\"?"))) param.max_qtty=stoi(m[2]);
        if (boost::regex_search(buf, m, boost::regex("max_share ?= ?\"?([0-9\.]+)\"?"))) param.max_share=stof(m[1]);
        if (boost::regex_search(buf, m, boost::regex("positive(_only)? ?= ?\"?((true)|(1))\"?"))) param.positive_only=true;
        if (boost::regex_search(buf, m, boost::regex("negative(_only)? ?= ?\"?((true)|(1))\"?"))) param.negative_only=true;
        if (boost::regex_search(buf, m, boost::regex("clone? ?= ?\"?((true)|(1))\"?"))) param.clone=true;
        if (boost::regex_search(buf, m, boost::regex("clone? ?= ?\"?((false)|(0))\"?"))) param.clone=false;
        if (boost::regex_search(buf, m, boost::regex("target ?= ?\"?\\{?([0-9,]+)\"?\\}?")))
        {
            std::string tmp=m[1];
            std::vector<std::string> strs=split(tmp, ',');
            for (auto i: strs)
            {
                param.target.push_back(stoi(i));
            }
        }
        if (boost::regex_search(buf, m, boost::regex("check"))) param.create_check=true;
    }
    fs->close();
    return param;
}


int cut_by_file(std::vector<std::string> arg)
{
    if (checkEnvInit()==false) return 1;
    if (arg.size()==0) {std::cerr << "No file specified" << std::endl; return 1; }
    std::fstream * fs=new std::fstream(arg[0]);
    if (!(*fs) or fs->eof()) {std::cerr << "Cannot open file " << arg[0]; return 1;}
    int res=env->cutToSets({parse_cut_param_file(fs)});
    delete fs;
    return res;
}

int reverse_poisonousness(std::vector<std::string> arg) {std::string dset=(arg.size()==0?"input":arg[0]); env->reverseVal(dset, 0); env->reverseVal(dset, 1); return 0;}
int nothing(std::vector<std::string> arg){return 0;}
int clear_data(std::vector<std::string>) {env->eraseAll(); return 0;}
int check(std::vector<std::string> args)
{
    env->eraseSet("file");
    std::fstream data("result");
    if (!data or data.eof()) std::cerr << "No result file" << "\r\n";
    else
        {env->addDescriptionsFromMachineFile(data, "file");}
    data.close();

    if (env->checkValues(args[2], args[1], args[0]))//finished correctly
        env->printStatistics();
    return 0;
}
int del_set(std::vector<std::string> arg)
{
    if (!checkEnvInit()) {std::cerr << "Environment not initialized yet, no datasets, nothing to delete\r" << std::endl; return -1;}
    if (arg.size()>0)
    {
        if (arg[0]!="")
        {
            #if OUTPUT_LEVEL>=VERBOSE_OUTPUT
                std::clog << "Deleting dataset " << arg[0] << std::endl;
            #endif // OUTPUT_LEVEL
            if (env->deleteSet(arg[0])) return 0;
            else return -1;
        }
    }
    std::cerr << "No dataset name specified\r" << std::endl;
    return -1;
}
int setsize(std::vector<std::string> arg)
{
    if (!checkEnvInit()) {std::cerr << "Environment not initialized yet, no datasets, nothing to measure\r" << std::endl; return -1;}
    if (arg.size()>0)
    {
        if (arg[0]!="") {std::cout << env->setsize(arg[0]); return 0;}
    }
    std::cerr << "No dataset name specified\r" << std::endl;
    return -1;
}
