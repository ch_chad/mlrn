#include "ObjDescription_str.h"

ObjDescription_str::ObjDescription_str()
{
    //ctor
}

ObjDescription_str::~ObjDescription_str()
{
    //dtor
}
unsigned int ObjDescription_str::addObject(std::string desc)
{
    m_descr.push_back(desc);
    return m_descr.size()-1;
}

std::string ObjDescription_str::getObject_str(unsigned int obj)
{
    if (obj>m_descr.size())
        return "";
    return m_descr[obj];
}
std::string ObjDescription_str::getValue(unsigned int obj, unsigned int attr)
{
    if (obj>m_descr.size() || attr>m_descr[obj].size())
    {
        std::cerr << "Out of bounds, no value" << "\r\n";
        return "";
    }
    return m_descr[obj][attr] + "";
}

unsigned int ObjDescription_str::size()
{
    return m_descr.size();
}
unsigned int ObjDescription_str::addObject(ObjDescription * orig_set, unsigned int orig_index)
{
    if (orig_set->size()<orig_index) return 1;
    addObject(orig_set->getObject_str(orig_index));
    return 0;
}
unsigned int ObjDescription_str::deleteObject(unsigned int index)
{
    if (index>=m_descr.size()) return 1;
    m_descr.erase(m_descr.begin()+index);
    return 0;
}
