#ifndef OBJDESCRIPTION_DBS_H
#define OBJDESCRIPTION_DBS_H

#include "ObjDescription.h"


class ObjDescription_dbs : public ObjDescription
{
    public:
        ObjDescription_dbs();
        ~ObjDescription_dbs();
        ObjDescription_dbs(ObjDescription_dbs * other):m_data(other->m_data) {};
//        template<>
        size_t addObject(boost::dynamic_bitset<> desc);
        size_t addObject(std::string desc);
        virtual size_t addObject(ObjDescription * orig_set, size_t orig_index);//copy
        virtual unsigned int deleteObject(size_t index);

        virtual std::string getValue(size_t obj, size_t attr);
        virtual size_t intersectPositive(size_t offset, boost::dynamic_bitset<> &res);
        virtual size_t size();
        virtual size_t descriptionLength() {if (m_data.size()==0) {std::cerr << "No data yet!" << "\r\n"; return 0;} return m_data[0].size();};
        virtual void purgeAttribute(size_t index) {modifyAllValues("0", index);};
        virtual void modifyAllValues(std::string nval_s, size_t start);
        virtual CompResult check(ObjDescription * obj, size_t obj_i, size_t index);
        virtual CompResult check(ObjDescription * obj, size_t obj_i, size_t index, size_t fbit, size_t lbit);
        virtual std::string getObject_str(size_t obj);
        virtual void reverseVal(size_t attr);
    protected:
        virtual boost::dynamic_bitset<> getObject_dbs(size_t obj);
    private:
        std::vector<boost::dynamic_bitset<> > m_data;

};

#endif // OBJDESCRIPTION_DBS_H
