#include "Encoder.h"
#include <iostream>
//#include <cstdlib> strtol(s.c_str(),0,10);
#include <string>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <iterator>
#include "main.h"

Encoder::~Encoder()
{
    m_attrs.clear();
}

void Encoder::printLatestAttr()
{
    printLatestAttr(m_attrs.size()-1);
}

void Encoder::printAttr(size_t level, size_t attr)
{
    if (level+1>m_attrs.size()) {std::cerr << "No such attribute level" << "\r\n";return;}
    if (attr+1>m_attrs[level]->size()) {std::cerr << "No such attribute" << "\r\n";return;}
    m_attrs[level]->printout(attr);

}

void Encoder::printLatestAttr(size_t level)
{
    if (level+1>m_attrs.size()) std::cerr << "No such attribute level" << "\r\n";
    else printAttr(level, m_attrs[level]->size()-1);
}


int Encoder::parseAttribute(std::vector<std::string> data, size_t start_line_num, size_t level, std::vector<std::string> &bit_descriptors)
{
    while (m_attrs.size()<=level)
    {
        //Attribute * n=new Attribute("NOTINAFF");
        //nv.push_back(n);
        m_attrs.push_back(new EncodingLevel());
    }

    boost::smatch m;
    boost::regex e ("^\>([^:]*)(: *([^\n]*))?$");
    std::string name("Unnamed");
    std::string type("binary");//just ordinal binary numbers
    std::string v_none("");
    std::string orig("");
    bool target(false);
    size_t i=0;
    for (; i<data.size(); i++)
    {
            if (boost::regex_match (data[i],m,e))
            {
                if ("name" == m[1])
                {
                    name = m[3];
                    continue;//next line
                }
                if ("copy" == m[1])
                {
                    orig=  m[3];
//                    std::clog << "COPYING " << orig << "\r\n";
                    continue;
                }
                if ("none" == m[1])
                {
                    v_none = m[3];
                    continue;
                }
                if ("type" == m[1])
                {
                    type=  m[3];
                    continue;//next line
                }
                if ("target" == m[1])
                {
                    target=true;
                    continue;//next line
                }
                if ("values" == m[1])//final part for all but copied, see just before the end
                {
                    i++;//offset must step over ">values" line
                    int res=m_attrs[level]->push_back_new(name, type, v_none, data, i, out_val_delim.length(), bit_descriptors, target);

                    //start_bit=m_attrs[level]->attrLastBitIndex(m_attrs[level]->size()-1)+1;
                    //printLatestAttr(1);
                    return res;

                }
                std::cerr << "Error in line " << start_line_num+i << " in encoding file: " << m[1] << " undefined " << "\r\n";
            }
            else //unmatched
                {
                    std::cerr << "Error in line " << start_line_num+i << " in encoding file: " << data[i] << " unexpected " << "\r\n";
                }
        }
        if ("" != orig)//and now for copied ones
        {
            int res=m_attrs[level]->push_back_copy(orig, target, v_none, bit_descriptors);
            //start_bit=m_attrs[level]->attrLastBitIndex(m_attrs[level]->size()-1)+1;
            return res;
        }
        std::cerr << "Error creating attribute " << name << ": neither 'values' nor 'copy' section exist" << "\r\n";
        m_attrs[level]->push_back_dummy(name);
    return 1;
}

std::string Encoder::encFnameStrippedComments(std::fstream& enc_dirty)
{
    std::clog << "Creating temp encoding file stripped of comments" << "\r\n";
    std::ofstream enc("encoding.tmp");
    if (enc_dirty)
    {
        boost::regex commentre( "/\\*.*?\\*/" ) ;//comment start and end, and as few characters in between as possible
        std::string stripped ;
        std::string code((std::istreambuf_iterator<char>(enc_dirty)), std::istreambuf_iterator<char>( ) ) ;
        enc_dirty.close( ) ;
        stripped = boost::regex_replace( code , commentre , "" ) ;
        stripped = boost::regex_replace(stripped, boost::regex("^([^/\n]+)//[^\n\r]*$"), "$1");//oneline comment not from the start of line
        stripped = boost::regex_replace(stripped, boost::regex("^//[^\n\r]*\n"), "");//oneline comment - a whole line
        stripped = boost::regex_replace(stripped, boost::regex("\n\n"), "\n");//no double \n
        stripped = boost::regex_replace(stripped, boost::regex(" $"), "");//and spaces that could be before comments
        enc << stripped;
        enc.close();
    }
    return "encoding.tmp";
}

/*Encoder::Encoder(std::fstream& enc_dirty):val_delim(" "), out_val_delim("")
{
        if (!enc_dirty or enc_dirty.eof()) {std::cerr << "No encoding file specified" << "\r\n";}
        else {getEncoding(enc_dirty);}
}*/

Encoder::Encoder(std::string fname_enc_dirty, std::string dummy_input_fname):val_delim(" "), out_val_delim("")
{
    fixSlashes(fname_enc_dirty);
    std::fstream enc_dirty(fname_enc_dirty);
    if (!enc_dirty or enc_dirty.eof())
    {
        std::cerr << "No encoding file specified, generating it automatically" << "\r\n"; dummyEncoding(fname_enc_dirty);
        enc_dirty.open(fname_enc_dirty);
        if (!enc_dirty or enc_dirty.eof())
        {
            std::cerr << "Error creating dummy encoding file. Filesystem related problems? Falling back to default encoding." << "\r" << std::endl;
            fname_enc_dirty=def_enc;
            dummyEncoding(fname_enc_dirty);
            enc_dirty.open(fname_enc_dirty);
        }
    }
    getEncoding(enc_dirty);
    if(dummy_input_fname=="")
    {
        std::fstream fs(def_in, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
        fs << generateHumanDummy() << std::endl;
        fs.close();
    }
    else
    {
        std::fstream fs(dummy_input_fname);
        if (!fs or fs.eof())
        {
            fs.close();
            fs.open(def_in, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
            fs << generateHumanDummy() << std::endl;
            fs.close();
        }
        //else we hope it's made correctly :)
    }
}

void Encoder::getEncoding(std::fstream& enc_dirty, size_t level)
{
    if (level==0) //cannot override bit coverage!
        level=std::max(m_attrs.size(), (size_t)1); //new level
    val_delim=" ";
    out_val_delim="";
    encFnameStrippedComments(enc_dirty);
    std::fstream enc("encoding.tmp");

    std::clog << "Acquiring encoding" << "\r\n";
    std::string line;
    size_t curr_line_num=0;
    size_t attr_qtty_promised=0;
    size_t attr_qtty_real=0;

    //size_t start_bit=0;

    std::vector<std::string> bit_descriptors;

    while (getline(enc, line))
    {
        curr_line_num++;
        if (">values delimiter:[" == line.substr(0, 19))
        {
            val_delim=line.substr(19, line.find("]")-line.find("[")-1);
            std::clog << "Delimiter set to {" << val_delim << "}" << "\r\n";
            if ("" == val_delim) std::cerr << "Warning! Value delimiter set to empty!" << "\r\n";
            continue;
        }
        if (">attributes quantity:" == line.substr(0, 21))
        {
            attr_qtty_promised=atoi(line.substr(21, line.size()).data());
            std::clog << attr_qtty_promised << " attributes promised" << "\r\n";
            continue;
        }
        if ("----" != line)
        {
            std::cerr << "Error in line " << curr_line_num << " in encoding file: " << line << " unexpected " << "\r\n";
            continue;
        }
        break;//----
    }

    //std::clog << m_attrs[level]->size() << std::endl;

    std::clog << "Starting parsing attributes" << "\r\n";
    std::vector<std::string> curr_attr;
    unsigned short errors_qtty=0;
    while (getline(enc, line))
    {
        curr_line_num++;
        if ("----" != line) curr_attr.push_back(line);
        else {
                errors_qtty+=parseAttribute(curr_attr, curr_line_num-curr_attr.size(), level, bit_descriptors);//try and parse what we've got
                attr_qtty_real++;
    /*            for (size_t i=0; i<bit_descriptors.size(); i++) std::clog << bit_descriptors[i] << " ";
    std::clog << "\r\n";*/
                curr_attr.clear();
                //printLatestAttr();
        }
    }
    if ("----" != line /*last ---- line omitted*/ && "" != line /*or an auto empty line inserted after it*/)
    {// hello code duplication!
        errors_qtty+=parseAttribute(curr_attr, curr_line_num-curr_attr.size(), level, bit_descriptors);//try and parse what we've got
        attr_qtty_real++;
        curr_attr.clear();
    }
    std::clog << "Encoding file over, success: " << attr_qtty_real-errors_qtty << " of " << attr_qtty_real << " attributes with " << attr_qtty_promised << " promised. " << (attr_qtty_promised>attr_qtty_real?"The file lacks some attributes.":(attr_qtty_promised<attr_qtty_real?"The file was bigger than expected. We still parsed it all.":""))<< "\r\n";


    //now update zero-level of attributes - bit descriptors
    #if OUTPUT_LEVEL >= ULTRA_OUTPUT
        std::clog << "bit descriptors: ";
        for (auto i: bit_descriptors) std::clog << i << " ";
        std::clog << "\r" << std::endl;
    #endif // OUTPUT_LEVEL
    m_attrs[BIT_COVERAGE]->updateBitDescriptors(bit_descriptors);
    m_attrs[BIT_COVERAGE]->copyTargetMarksForBitDescriptors(m_attrs[level]);
    //printBitEncoding();
}

std::string Encoder::convert(std::string data, Direction direction, size_t level)
{
    if (m_attrs.size()<=level) {std::cerr << "No such attribute level, aborting conversion" << "\r\n"; return "ERROR";}
    std::string encdata("");
    std::vector<std::string> splitres;
    #if OUTPUT_LEVEL >= ULTRA_OUTPUT
        std::clog << "converting [" << data << "]" << "\r" << std::endl;
    #endif
    if (direction==Direction::decoding and "" == out_val_delim)
    {//only by offsets!
        if (data.length()<m_attrs[level]->attrLastBitIndex(m_attrs[level]->size()-1)) {std::cerr << "Cannot decode: string too short" << "\r\n"; return "";}
        for (size_t i=0; i<m_attrs[level]->size(); i++)
        {
            #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                std::clog << m_attrs[level][i]->attrname() << " " << m_attrs[level][i]->firstBit() << ", " << m_attrs[level][i]->lastBit() << ": [" << data.substr(m_attrs[level][i]->firstBit(), m_attrs[level][i]->lastBit()-m_attrs[level][i]->firstBit()+1) << "], that means " << m_attrs[level][i]->decode(data.substr(m_attrs[level][i]->firstBit(), m_attrs[level][i]->lastBit()-m_attrs[level][i]->firstBit()+1)) << "\r" << std::endl;
            #endif
            splitres.push_back(data.substr(m_attrs[level]->attrFirstBitIndex(i), m_attrs[level]->attrLastBitIndex(i)-m_attrs[level]->attrFirstBitIndex(i)+1));
        }
    }
    else if (direction==Direction::encoding and "" == val_delim)
    {
        std::cerr << "Encoding with empty input delimiter not implemented yet. Why don't you use some other input file format? That's a complex task, what did you expect - search for possible values relying on prefix code thesis? Or full recursive search?\r" << std::endl;
        return "";
    }//else delimiters not empty
    else boost::split_regex(splitres, data, boost::regex((direction==Direction::encoding?val_delim:out_val_delim)));

    if (splitres.size() != m_attrs[level]->size())
    {
        std::cerr << "Incorrect input length (" << splitres.size() << ", " << m_attrs[level]->size() << " expected), object ignored." <<
                (splitres.size()>m_attrs[level]->size()&&data.find(val_delim+val_delim)!=std::string::npos?" Double delimiter detected, interpreted as an empty value, is it okay?":"") << "\r\n";
        return "";
    }
    for (size_t i=0; i<splitres.size(); i++)
    {
        #if OUTPUT_LEVEL>=ULTRA_OUTPUT
            std::clog << "Description split okay, now decoding its parts\r" << std::endl;
        #endif // OUTPUT_LEVEL
        std::string nval=(direction==Direction::decoding?m_attrs[level]->decode(i, splitres[i])+val_delim:m_attrs[level]->encode(i, splitres[i])+out_val_delim);

        if (nval==val_delim or nval==out_val_delim/*:) too lazy to implement serious checks, hope you didnt use semicolumns or such as a value*/)
        {
            #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                std::cerr << m_attrs[level]->decode(i, splitres[i]) << "\r\n";
            #endif
            std::cerr << "Value '" << splitres[i] << "' undefined, object ignored" << "\r\n";
            return "";
        }
        encdata+=nval;
    }
    //cut off last delimiter
    encdata=encdata.substr(0, encdata.size()-(direction==Direction::encoding?out_val_delim:val_delim).size());
    return encdata;
}

std::string Encoder::decode(std::string desc, size_t level)
{
    return convert(desc, Direction::decoding, level);
}

std::string Encoder::encode(std::string desc, size_t level)
{
    return convert(desc, Direction::encoding, level);
}


int Encoder::encodeAndSave(std::string desc, ObjDescription * p_input, size_t level)
{
    std::string str=encode(desc, level);
    if (str=="")
    {
        std::cerr << "Conversion failed" << "\r\n";
        return 1;
    }
    else p_input->addObject(str);
    //std::clog << "Object encoded and sent to input set: " << str << "\r\n";
    return 0;
}

int Encoder::printHumanInput(ObjDescription * p_input, size_t level, bool conv)
{
    if (p_input == NULL)
    {
        std::cerr << "No data for decoding and printing out" << "\r\n";
        return 1;
    }
    for (size_t i=0; i<p_input->size(); i++)
    {//printing one description
        std::cout << "Object " << i+1 << ": " << "\r\n";
        std::clog << p_input->getObject_str(i) << "\r\n";
        if (conv) std::cout << decode(p_input->getObject_str(i), level) << "\r\n";
        std::cout << "------------" << "\r\n";
    }
//    std::cout << "!!! " << m_attrs[0][0]->firstBit() << " " << m_attrs[0][0]->attrname() << "\r\n";
    return 0;
//    std::cout << convert(p_input->getObject(0), Direction::decoding) << "\r\n";
}



std::string Encoder::howToEncode(std::string attr, std::string human_value, size_t &start)
{
    std::string res="";
    for (size_t i=0; i<m_attrs.size(); i++)
    {
        res=m_attrs[i]->howToEncode(attr, human_value, start);
        if (res!="")
        {
            #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                std::clog << human_value << " should be encoded as " << res << ", starting from bit " << start << "\r" << std::endl;
            #endif // OUTPUT_LEVEL
            return res;
        }
    }
    std::cerr << "Attribute " << attr << " with value " << human_value << " not found\r" << std::endl;
    return "";

}

bool Encoder::findAttribute(std::string name, std::string posval, size_t &level, size_t &attr)
{
    bool found=false;
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Looking for attribute " << name << " with posval " << posval<<std::endl;
    #endif // OUTPUT_LEVEL
    for (size_t i=0; i<m_attrs.size(); i++)
    {
        found=m_attrs[i]->findAttribute(name, posval, attr);
        if (found) {level=i; return true;}
    }
    return false;
}

bool Encoder::checkValues(ObjDescription * okay, std::string attr_n, ObjDescription * cand, size_t (&results)[4])
{
    //cand consists of only one target attribute
    if (okay->size()==0 or okay->descriptionLength()==0) {std::cerr << "No data to check with, aborting.\r" << std::endl; return false;}
    if (cand->size()==0 or cand->descriptionLength()==0) {std::cerr << "No data to check, aborting.\r" << std::endl; return false;}
    if (okay->size() != cand->size()) {std::cerr << "Different number of objects in correct and predicted datasets, don't know how to compare, aborting.\r" << std::endl; return false;}


    size_t level=0, attr=0;
    if (!findAttribute(attr_n, "", level, attr)) {std::cerr << "No attribute "<<attr_n<<", cannot compare" << "\r\n"; return false;}

    size_t lbit=supposeEncLength()-1-m_attrs[level]->attrFirstBitIndex(attr), fbit=supposeEncLength()-1-m_attrs[level]->attrLastBitIndex(attr);
    if (lbit-fbit+1!=cand->descriptionLength()) {std::cerr << "Incorrect description length in candidate dataset: " << cand->descriptionLength() << " with " << lbit-fbit+1 << " expected" << "\r\n"; return false;}

    size_t total=okay->size();
    for (size_t i=0; i<total; i++)
    {
        results[okay->check(cand, i, i, fbit, lbit)]++;
    }
    return true;

}
/*std::string Encoder::howToEncode(std::string attr, std::string human_value, size_t &start)
{
    std::string res;
    for (size_t i=0; i<m_attrs.size(); i++)
    {
        res=m_attrs[i]->howToEncode(attr, human_value, start);
        if (res!="") return res;
    }
    std::cerr << "Cannot find attribute " << attr << ", encoding attempt failed\r" << std::endl;
    return "";
}*/
std::string Encoder::generateHumanDummy()
{
        if (!initialized() or m_attrs.size()<2)
        {
            std::cerr << "Encoder not initialized yet" << std::endl;
            return "";
        }
        std::ostringstream buf;
        for (size_t i=0; i< m_attrs[1]->size(); i++)
        {
            buf<<m_attrs[1]->none(i)<<(i==0?"":val_delim);
        }
        return buf.str();
    }

void fixSlashes(std::string &fname_enc_dirty);
void fixSlashes(std::string &fname_enc_dirty)
{
    while (fname_enc_dirty.find("\\")!=std::string::npos)
    {
        #if OUTPUT_LEVEL >= MEDIUM_OUTPUT
            std::clog << "Fixed wrong-sided slash in a filename. Please don't use it again.\r" << std::endl;
        #endif
        fname_enc_dirty[fname_enc_dirty.find("\\")]='/';
    }
}
