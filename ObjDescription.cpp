#include "ObjDescription.h"

ObjDescription::ObjDescription()
{
    //ctor
}

ObjDescription::~ObjDescription()
{
    //dtor
}

ObjDescription::ObjDescription(const ObjDescription& other)
{
    //copy ctor
}

ObjDescription& ObjDescription::operator=(const ObjDescription& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

/*int ObjDescription::printAll()
{
    std::clog << "Printing all descriptions" << "\r\n";
    size_t i=0;
    for(i=0; i<size(); i++)
    {
        std::cout << "Element " << i << " : " << getObject_str(i) << "\r\n";
    }
    std::clog << "Printout over, " << i << " elements" << "\r\n";
    return i;
}*/
