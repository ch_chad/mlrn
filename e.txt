>values delimiter:[,]
>attributes quantity: 23
----
>name: edible
>type: selective
>target
>none: *
>values:
e //edible
p //poisonous
----
>name: cap_shape
>type: tree
>values:
b<c //bell
c< x //conical
x //convex
f //flat
k <f //knobbed
s < k //sunken
----
>name: cap_surface
>type: selective
>values:
f //fibrous
g //grooves
y //scaly
s //smooth
----
>name:cap_color
>type: tree
>values:
n<b //brown
b<c //buff
c //cinnamon
g //gray
r //green
p //pink
u<p //purple
e<p //red
w //white
y //yellow
----
>name: bruises
>type: selective
>values:
t
f
----
>name: odor
>type: selective
>values:
a //almond
l //anise
c //creosote
y //fishy
f //foul
m //musty
n //none
p //pungent
s //spicy
----
>name:gill-attachment
>type: selective
>values:
a //attached
d //descending
f //free
n //notched
----
>name:gill-spacing
>type: selective
>values:
c //close
w //crowded
d //distant
----
>name:gill-size
>type: selective
>values:
b //broad
n //narrow
----
>name: gill-color
>type:selective
>values:
k //black
n //brown
b //buff
h //chocolate
g //gray
r //green
o //orange
p //pink
u //purple
e //red
w //white
y //yellow
----
>name: stalk-shape
>type: selective
>values:
e //enlarging
t //tapering
----
>name: stalk-root
>type: selective
>values:
b //bulbous
c //club
u //cup
e //equal
z //rhizomorphs
r //rooted
? //missing
----
>name: stalk-surface-above-ring
>type: selective
>values:
f //fibrous
y //scaly
k //silky
s //smooth
----
>name: stalk-surface-below-ring
>type: selective
>values:
f //fibrous
y //scaly
k //silky
s //smooth
----
>name: stalk-color-above-ring
>type: selective
>values:
n //brown
b //buff
c //cinnamon
g //gray
o //orange
p //pink
e //red
w //white
y //yellow
----
>name: stalk-color-below-ring
>type: selective
>values:
n //brown
b //buff
c //cinnamon
g //gray
o //orange
p //pink
e //red
w //white
y //yellow
----
>name: veil-type
>type: selective
>values:
p //partial
u //universal
----
>name: veil-color
>type: selective
>values:
n //brown
o //orange
w //white
y //yellow
----
>name: ring-number
>type: selective
>values:
n //none
o //one
t //two
----
>name: ring-type
>type: selective
>values:
c //cobwebby
e //evanescent
f //flaring
l //large
n //none
p //pendant
s //sheathing
z //zone
----
>name: spore-print-color
>type: selective
>values:
k //black
n //brown
b //buff
h //chocolate
r //green
o //orange
u //purple
w //white
y //yellow
----
>name: population
>type: selective
>values:
a //abundant
c //clustered
n //numerous
s //scattered
v //several
y //solitary
----
>name: habitat
>type: selective
>values:
g //grasses
l //leaves
m //meadows
p //paths
u //urban
w //waste
d //woods
----
