#ifndef OBJDESCRIPTION_H
#define OBJDESCRIPTION_H
#include <string>
#include <vector>
#include <iostream>
#include "boost/dynamic_bitset.hpp"
#include "output_levels.h"

class ObjDescription_dbs;

enum CompResult
{
    Okay,
    ShouldntVBeenOkay,
    ShouldVBeenOkay,
    Error
};

class ObjDescription
{
    public:
        /*enum Special
        {
            UNKNOWN
        };*/
        ObjDescription();
        virtual ~ObjDescription();
        ObjDescription(const ObjDescription& other);
        ObjDescription& operator=(const ObjDescription& other);
        virtual size_t addObject(std::string desc)=0;
        virtual size_t addObject(boost::dynamic_bitset<> desc)=0;
        /*template <typename OBJ_TYPE, typename OBJ_ID>
            OBJ_TYPE getObject(OBJ_ID obj);
        template <typename OBJ_ID, typename ATTR_ID, typename VAL_TYPE>
            VAL_TYPE getValue(OBJ_ID obj, ATTR_ID attr);*/
        virtual size_t addObject(ObjDescription * orig_set, size_t orig_index)=0;//copy
        virtual unsigned int deleteObject(size_t index)=0;

        virtual std::string getValue(size_t obj, size_t attr)=0;
        virtual size_t intersectPositive(size_t offset, boost::dynamic_bitset<> &res)=0;
        virtual size_t descriptionLength()=0;
//        int printAll();
        virtual size_t size()=0;
        virtual void reverseVal(size_t attr)=0;
        virtual void purgeAttribute(size_t index)=0;
        virtual void modifyAllValues(std::string nval_s, size_t start)=0;
        virtual void printAll() {for (size_t i=0; i<size(); i++) print(i);};
        virtual void print(size_t obj) {std::cout << getObject_str(obj) << "\r\n";};
        virtual CompResult check(ObjDescription * obj, size_t obj_i, size_t index)=0;
        virtual CompResult check(ObjDescription * obj, size_t obj_i, size_t index, size_t fbit, size_t lbit)=0;
        virtual std::string getObject_str(size_t obj)=0;
        friend ObjDescription_dbs;//for getObject_dbs inside copying
    protected:
        virtual boost::dynamic_bitset<> getObject_dbs(size_t obj)=0;// {std::cerr << "No conversion from string to dynamic bitset yet" << "\r\n"; return (boost::dynamic_bitset<>(getObject_str(0).length(), pow(2, getObject_str(0).length())-1));/*all NONEs*/}

};

#endif // OBJDESCRIPTION_H
