#ifndef ANALYZER_TIMER_H
#define ANALYZER_TIMER_H

#include "Analyzer.h"


class Analyzer_Timer : public Analyzer
{
    public:
        Analyzer_Timer();
        ~Analyzer_Timer();
        virtual void prepare();
        virtual void stop();
        virtual std::string sumup(ObjDescription* result, ObjDescription* desired);

    protected:
    private:
};

#endif // ANALYZER_TIMER_H
