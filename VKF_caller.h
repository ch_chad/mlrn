#ifndef VKF_CALLER_H
#define VKF_CALLER_H

#include "MLMethod.h"


class VKF_caller : public MLMethod
{
    public:
        VKF_caller();
        virtual ~VKF_caller();
        virtual void proceed (ObjDescription* in, ObjDescription* out);
        virtual void proceed (std::string filein, std::string filepredict, std::string fileres);
        virtual bool isProbabilistic() {return true;};
    protected:
    private:
};

#endif // VKF_CALLER_H
