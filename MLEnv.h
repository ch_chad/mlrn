#ifndef MLENV_H
#define MLENV_H
#include "Encoder.h"
#include "ObjDescription_dbs.h"
#include "MLMethod.h"
#include <fstream>
#include <map>
//#include "DependencyFinder.h"
#include <limits>
#include "VKF_caller.h"
#include "MLEnvParams.h"
#include "DataPart.h"


class MLEnv
{
    public:
//        MLEnv(std::fstream &input, std::fstream &enc);
        //MLEnv(std::fstream &enc);
        MLEnv(std::string fname_enc, std::string fname_input="");
        ~MLEnv();
        bool encoder_okay() {return m_encoder->initialized();};
        MLEnv(const MLEnv&);//pointers!
        MLEnv operator=(const MLEnv&);//pointers!
        void proceed();
        std::string getStat();
        int addDescriptionsFromHumanFile(std::fstream &f, std::string setname="");
        int addDescriptionsFromMachineFile(std::fstream &f, std::string setname="");
        size_t cutToSets(std::vector< DataPart > params);
        int printDataset(std::string setname="", bool conv=true);
        int listDatasets(){if (checkInit()==false) return 1; if(m_data.size()==0) {std::cout << "No datasets yet" << std::endl; return 0;} for (auto i:m_data) std::cout << i.first << std::endl; return 0;};
        int sendSetToFile(std::string setname, std::fstream &f, bool human=false);
        int sendSetToFile(std::string setname, std::string fname, bool human=false)
        {
                #if OUTPUT_LEVEL>=MEDIUM_OUTPUT
                        std::clog << "Sending set " << setname << " to file " << fname << "\r" << std::endl;
                #endif
                std::fstream fs(fname, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
                return sendSetToFile(setname, fs, human);
        };
        int sendSetsToFiles(bool human=false)
        {
            for (auto i:m_data)
            {
                sendSetToFile(i.first, i.first, human);
            }
            return 0;
        };
        void eraseAll();
        std::map<std::string, ObjDescription *>::iterator eraseSet(std::string setname);
        bool deleteSet(std::string setname);
        void deleteSetsByPrefix(std::string prefix);
//        void findDependencies() {m_df.supposeDependencies(m_data["input"]);};
        void cloneSet(std::string set_from, std::string set_to);
        bool purgeTargets(std::string where, std::vector<size_t> which={});
        int printEncoding(size_t level=0, std::fstream *fs=NULL){return m_encoder->printEncoding(level, fs);};
        void setAllValues(std::string dataset, std::string attr, std::string human_value);
        void compareSets(std::string okay_n, std::string cand_n);//both full
        bool checkValues(std::string okay_n, std::string attr, std::string cand_n);//cand has only one attribute
        void printStatistics() {for (std::map<std::string, std::string>::iterator it=m_statistics.begin(); it!= m_statistics.end(); it++) std::cout << it->first << " : " << it->second << "\r\n";};
        void deleteAttribute(std::string attr_n);
        //void deleteAttributesBut(std::string attr_n) {/*size_t level=0; Attribute * attr=m_encoder->findAttributeAndLevel(attr_n, level); if (attr==NULL) {std::cerr << "No attribute to spare, aborting attribute deletion" << "\r\n"; return;} /*TODO*/}
        bool estimateDataSizeForAccuracy();
        void VKFit(std::string learn="",  std::string predict="", std::string result="") {MLMethod * method=new VKF_caller(); method->proceed(learn, predict, result);};

        size_t datasetsQtty() {return m_data.size();};
        void findDependenciesWithOuterPredictor();
        void reverseVal(std::string dset, size_t attr);
        void make_default_enc(std::string fname) {params.dummyEnc(fname);};
        size_t setsize(std::string setname)
        {
                if (checkInit())
                {
                        if (m_data.size()==0)
                        {
                                std::cerr << "No datasets yet, nothing to measure\r" << std::endl;
                                return 0;
                        }
                        else
                        {
                                if (m_data.find(setname)!=m_data.end())
                                        return m_data[setname]->size();
                                std::cerr << "No such dataset\r" << std::endl;
                                return 0;
                        }
                }
                std::cerr << "Not initialized yet\r" << std::endl;
                return 0;
        }

    protected:
    private:
        MLEnvParams params;
        bool these_positive(size_t obj, std::vector <size_t> attrs);
        bool checkInit() {if (this==NULL) {std::cerr << "Environment not initialized yet" << std::endl; return false;} return true;};
        bool DataSizeForAcc_GeneratePredictAndCheck(std::string prefix_desired="desired/", std::string prefix_check="check/", size_t check_qtty=1);
        bool DataSizeForAcc_GenerateLearnSet(std::string prefix_input="input/", size_t input_size=1);
        bool DataSizeForAcc_adviseSize(std::vector<std::vector<float> > accuracy, size_t min_input_size);
        std::string getFreeSetName();
        /*enum Qtty
        {
            NotEnough,
            Okay,
            TooMany
        };*/

        std::fstream * f_input;
        MLEnv();
        std::map<std::string, ObjDescription*> m_data;
//        unsigned int input_total() {if (checkInit()==false) return 0; return m_data["input"]->size();};//TODO:error if no such dataset
        Encoder * m_encoder;
        std::map<std::string, std::string> m_statistics;
//        DependencyFinder m_df;
        MLMethod * m_method=NULL;
        ObjDescription * cutASet(DataPart param, size_t itotal, ObjDescription * check=NULL);

};

#endif // MLENV_H
