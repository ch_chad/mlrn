#include "MLEnv.h"
#include <stdlib.h>
#include <limits>
#include <time.h>
#include <sstream>
#include <iostream>
#include <chrono>
#include <thread>
MLEnv::~MLEnv()
{
//    delete m_data;
    //delete m_method;
    for (auto i: m_data)    delete i.second;
    delete m_encoder;
    delete f_input;
    delete m_method;
}

/*MLEnv::MLEnv(std::fstream &input, std::fstream &enc): f_input(&input), m_encoder(new Encoder(enc)), m_df(m_encoder), params("settings.txt")
{
    m_data["input"]=new ObjDescription_dbs();
    m_data["output"]=new ObjDescription_dbs();
    m_data["desired"]=new ObjDescription_dbs();
    m_statistics["general"]="nothing";
}*/

MLEnv::MLEnv(std::string fname_enc, std::string fname_input): params(fname_enc, fname_input), f_input(new std::fstream(fname_input)), m_encoder(new Encoder(fname_enc))/*m_df(m_encoder), */
{
/*    if (fname_input=="") m_encoder->generateHumanDummy();
    std::fstream fs(fname_input);
    if (!fs or fs.eof()) m_encoder->generateHumanDummy();
    fs.close();*/
//    m_data["input"]=new ObjDescription_dbs();
//    m_data["output"]=new ObjDescription_dbs();
//    m_data["desired"]=new ObjDescription_dbs();
    m_statistics["general"]="nothing";
}


/*MLEnv::MLEnv(std::fstream &enc): f_input(NULL), m_encoder(new Encoder(enc)), m_df(m_encoder), params("settings.txt")
{
    m_data["input"]=new ObjDescription_dbs();
    m_data["output"]=new ObjDescription_dbs();
    m_data["desired"]=new ObjDescription_dbs();
    m_statistics["general"]="nothing";
}*/


std::string MLEnv::getFreeSetName()
{
    std::string setname="spam";
    while (m_data.find(setname)!=m_data.end())
    {
        setname=setname+", spam";
    }
    return setname;
}

int MLEnv::addDescriptionsFromHumanFile(std::fstream &f, std::string sname)
{
    if (!f or f.eof())
    {
        std::cerr << "Given empty input file, aborting.\r" << std::endl;
        return 1;
    }
    if (checkInit()==false) return 1;
    std::string setname=(sname==""?getFreeSetName():sname);
    if (m_data.find(setname)==m_data.end()) m_data[setname]=new ObjDescription_dbs();
    std::string readobject;
    std::clog << "Started reading input set" << "\r\n";
    size_t errors=0;
    while (getline(f, readobject))
    {
        errors+=m_encoder->encodeAndSave(readobject, m_data[setname], 1);
    }
    std::clog << "Input set over. Errors: " << errors << "\r\n";
    return 0;
}

void MLEnv::cloneSet(std::string set_from, std::string set_to)
{
    if(m_data.find(set_from)==m_data.end()) {std::cerr << "Dataset [" << set_from << "] for copying not found" << "\r\n"; return;}
    m_data[set_to]=new ObjDescription_dbs((ObjDescription_dbs*)m_data[set_from]);
}
bool MLEnv::purgeTargets(std::string where, std::vector<size_t> which)
{
    if (m_data.find(where)==m_data.end())
    {
        std::cerr << "No such dataset, cannot purge targets\r\n";
        return false;
    }
    std::vector<size_t> target_indeces;
    if (which.size()==0) m_encoder->getTargetIndeces(target_indeces);
    else
    {
        size_t l=m_encoder->supposeEncLength()-1;
        for(size_t i=0; i<which.size(); i++)
        {target_indeces.push_back(l-which[i]);}
    }
    for (size_t i=0; i<target_indeces.size(); i++)
    {
        m_data[where]->purgeAttribute(target_indeces[i]);
    }
    return true;
}

int MLEnv::addDescriptionsFromMachineFile(std::fstream &f, std::string sname)
{
    if (checkInit()==false) return 1;
    std::string setname=(sname==""?getFreeSetName():sname);
    if (m_data.find(setname)==m_data.end()) m_data[setname]=new ObjDescription_dbs();
    std::string readobject;
    std::clog << "Started reading input set" << "\r\n";
    size_t lines=0, prev_set_size=m_data[setname]->size();
    while (getline(f, readobject))
    {
        m_data[setname]->addObject(readobject);
        lines++;
    }
    std::clog << "Input set over. Errors: " << lines-m_data[setname]->size()-prev_set_size << "\r\n";
    return 0;
}

void MLEnv::eraseAll()
{
    for (auto i: m_data) delete i.second;
    m_data.erase(m_data.begin(), m_data.end());
}
std::map<std::string, ObjDescription *>::iterator MLEnv::eraseSet(std::string setname)
{
    auto od=m_data.find(setname);
    if (od!=m_data.end()) {delete od->second; od=m_data.erase(od);return od;}
    else std::cerr << "No such dataset - nothing to erase" << "\r\n";
    return m_data.end();
}

int MLEnv::printDataset(std::string setname, bool conv)
{
    if (setname=="")
    {
        for (auto it:m_data)
        {
            std::clog << "Human-readable " << it.first << " data:" <<"\r\n";
            //ObjDescription* o=it.second;
            m_encoder->printHumanInput(it.second, 1, conv);
        }
    }
    else
    {
        std::clog << "Human-readable " << setname << " data:" <<"\r\n";
        auto dataset=m_data.find(setname);
        if (dataset==m_data.end()) std::cerr << "Dataset not found" << "\r\n";
        else m_encoder->printHumanInput((*dataset).second, 1, conv);
    }
    return 0;
}

void MLEnv::proceed()
{
    m_data["input"]->printAll();
//    m_analyzer.prepare();
//    m_method->proceed(m_input, m_output);
//    m_analyzer.stop();
//    m_statistics=m_analyzer.sumup(m_output, m_desired);
}

/*std::string MLEnv::getStat()
{
    return m_statistics;
}*/

bool MLEnv::these_positive(size_t obj, std::vector <size_t> attrs)
{
    for (auto i: attrs)
    {
        if (i>m_encoder->supposeEncLength()-1) {std::cerr << "Cannot check positiveness, no such attribute\r" << std::endl; return false;}
        if (m_data["input"]->getValue(obj, i)=="0") return false;
    }
    return true;
}

ObjDescription * MLEnv::cutASet(DataPart param, size_t itotal, ObjDescription * check)
{
    size_t min_qtty=std::max(param.min_qtty, (size_t) (param.min_share*itotal));
    size_t max_qtty=std::min(param.max_qtty, (size_t) (param.max_share*itotal));
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Expected from " << min_qtty << " to " << max_qtty << " elements" << "\r\n";
    #endif
    if (min_qtty>setsize("input"))
    {
        std::cerr << "Too many data asked, leaving empty" << "\r\n";
        return NULL;
    }
    if (min_qtty>max_qtty)
    {
        std::cerr << "Minimum quantity of elements desired is bigger than maximum quantity, leaving empty" << "\r\n";
        return NULL;
    }
    if (max_qtty==0)
    {
        std::cerr << "Empty set asked" << "\r\n";
        return NULL;
    }
    ObjDescription * res=new ObjDescription_dbs();
    while(true)//not very good actually: always stops at minimum
    {
        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Cycling through input, " << res->size() << " objects so far\r" << std::endl;
        #endif
        for (size_t j=0; j<m_data["input"]->size() and (res->size()<max_qtty or 0==max_qtty); j++)//while maximum not hit
        {
            if (rand()%100<=param.prob*100 and \
                (param.target.size()==0 or (param.positive_only==false and param.negative_only==false) or \
                    ((param.positive_only==true and these_positive(j, param.target)) or \
                    (param.negative_only==true and !these_positive(j, param.target))
                    )))
            {
                res->addObject(m_data["input"], j);
                if (param.create_check and check!=NULL) check->addObject(m_data["input"], j);
                if (!param.clone) m_data["input"]->deleteObject(j);
                #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                    std::clog << "Element #" << j << " moved from input to " << curset << "\r\n";
                #endif
            }
        }
        if (res->size()>=min_qtty)
        {
            #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                std::clog << "Minimum hit, set ready\r\n";
            #endif
            return res;
        }
    }
}

size_t MLEnv::cutToSets(std::vector< DataPart > params)
{
    if (checkInit()==false) return 0;
    std::srand(time(NULL));
    size_t itotal=setsize("input");//YES. size of m_data is changing during here
    size_t created_sets=0;
    for (size_t i=0; i<params.size(); i++)
    {//all parts, i.e. desired, input1...
        if (params[i].prob==0) {std::cerr << "Zero probability for moving sample to another set! Changed to 0.1: you wouldn't want this to work forever." << "\r\n"; params[i].prob=0.1;}
        if (params[i].positive_only==true and params[i].negative_only==true) {std::cerr << "Datasets cannot be positive-only and negative-only at the same time... That is, they can, but only if they are empty.\r" << std::endl;}
        std::string curset=params[i].name;
        std::string checkname=params[i].name+"_check";
        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Filling " << curset << " dataset" << "\r\n";
        #endif
        ObjDescription * chk=(params[i].create_check==true?new ObjDescription_dbs():NULL);
        ObjDescription * tmp=cutASet(params[i], itotal, chk);
        if (tmp==NULL) std::cerr << "Error populating dataset " << curset << ", it won't be created" << "\r\n";
        else
        {
            m_data[curset]=tmp;
            created_sets++;
            if (params[i].create_check==true)
            {
                m_data[checkname]=chk;
                created_sets++;
                purgeTargets(curset, params[i].target);
            }
        }

    }
    return created_sets;
}

int MLEnv::sendSetToFile(std::string setname, std::fstream &f, bool human)
{
    if (!f) {std::cerr << "Error opening file for writing dataset\r"<<std::endl; return 1;}
    if (m_data.find(setname)==m_data.end()) {std::cerr << "No dataset "<< setname<< ", nothing to set to file" << "\r\n"; return -1;}
    for (size_t i=0; i<m_data[setname]->size(); i++)
    {
        f << (human?m_encoder->human(m_data[setname]->getObject_str(i)):m_data[setname]->getObject_str(i)) << "\r" << std::endl;
    }
    return m_data[setname]->size();
}

void MLEnv::compareSets(std::string okay_n, std::string cand_n)
{
    if (m_data.find(okay_n)==m_data.end() or m_data.find(cand_n)==m_data.end()) {std::cerr << "No such dataset" << "\r\n"; m_statistics["general"]="uncompareable";return;}
    if (m_data[okay_n]->size()!=m_data[cand_n]->size()) {std::cout << "Sizes are different, cannot compare" << "\r\n"; m_statistics["general"]="uncompareable";return;}
    size_t total=m_data[okay_n]->size();
    size_t results[4]={0,0,0,0};
    for (size_t i=0; i<total; i++)
    {
        results[m_data[okay_n]->check(m_data[cand_n], i, i)]++;
    }
    m_statistics["general"]=(results[CompResult::Okay]<total?"ready":"HEADSHOT!");
    m_statistics["false positives share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldntVBeenOkay]/(float)total) )->str();
    m_statistics["false negatives share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldVBeenOkay]/(float)total) )->str();
    m_statistics["false positives qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldntVBeenOkay]) )->str();
    m_statistics["false negatives qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldVBeenOkay]) )->str();
    m_statistics["correct predictions share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::Okay]/(float)total) )->str();
    m_statistics["correct predictions qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::Okay]) )->str();
    m_statistics["total"]=static_cast<std::ostringstream*>( &(std::ostringstream() << total) )->str();
}

bool MLEnv::checkValues(std::string okay_n, std::string attr_n, std::string cand_n)//cand has only one attribute, that is 'attr' in okay set
{
    m_statistics["general"]="uncompareable";
    //std::cout << (*m_data.find(okay_n)).first << " " << (*m_data.find(cand_n)).first << "\r\n";
    if (m_data.find(okay_n)==m_data.end()) {std::cerr << "Dataset specified as correct not found - " << okay_n << "\r\n"; m_statistics["general"]="uncompareable";return false;}
    if (m_data.find(cand_n)==m_data.end()) {std::cerr << "Dataset specified for checking not found - " << cand_n << "\r\n"; m_statistics["general"]="uncompareable";return false;}
    if (m_data[okay_n]->size()!=m_data[cand_n]->size()) {std::cerr<< "Sizes are different, cannot compare (given " << m_data[cand_n]->size() << " with correct "<< m_data[okay_n]->size()<< ")\r\n"; m_statistics["general"]="uncompareable";return false;}

    size_t results[4]={0,0,0,0};
    size_t total=m_data[okay_n]->size();
    if (m_encoder->checkValues(m_data[okay_n], attr_n, m_data[cand_n], results)==false) {std::cerr << "Error comparing datasets!\r" << std::endl; return false;}
    m_statistics["general"]=(results[CompResult::Okay]<total?"ready":"HEADSHOT!");
    //below: just on-the-fly conversions from float to str, nevermind
    m_statistics["false positives share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldntVBeenOkay]/(float)total) )->str();
    m_statistics["false negatives share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldVBeenOkay]/(float)total) )->str();
    m_statistics["false positives qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldntVBeenOkay]) )->str();
    m_statistics["false negatives qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::ShouldVBeenOkay]) )->str();
    m_statistics["correct predictions share"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::Okay]/(float)total) )->str();
    m_statistics["correct predictions qtty"]=static_cast<std::ostringstream*>( &(std::ostringstream() << results[CompResult::Okay]) )->str();
    m_statistics["total"]=static_cast<std::ostringstream*>( &(std::ostringstream() << total) )->str();
    return true;
}

bool MLEnv::DataSizeForAcc_GenerateLearnSet(std::string prefix_input, size_t input_size)
{
    std::vector<DataPart> param(1);
    std::ostringstream s, s1;

    param[0].prob=0.1;
    param[0].min_qtty=input_size;
    param[0].max_qtty=input_size;
    s.str("");
    s << prefix_input<< input_size;
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Generating dataset " << s.str() << "\r\n";
    #endif
    eraseSet(s.str());
    param[0].name=s.str();
    param[0].clone=true;

    if (cutToSets(param) == 1) return true;
    std::cerr << "Input size estimation interrupted: error cutting some learning set\r\n";
    return false;
}

bool MLEnv::DataSizeForAcc_GeneratePredictAndCheck(std::string prefix_desired, std::string prefix_check, size_t check_qtty)
{
    std::ostringstream s, s1;
    deleteSetsByPrefix(prefix_desired);
    deleteSetsByPrefix(prefix_check);

    #if OUTPUT_LEVEL >= MEDIUM_OUTPUT
        std::clog << "Generating predict and check datasets\r\n";
    #endif
    std::vector<DataPart> param_des(check_qtty);
    for (size_t i=0; i<check_qtty; i++)
    {
        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Cutting set #" << i << "\r\n";
        #endif
        param_des[i].min_share=0.1;
        param_des[i].max_share=0.2;
        param_des[i].prob=0.1;
        s.str("");
        s<<prefix_desired<<i;
        param_des[i].name=s.str();
        param_des[i].clone=true;
    }
    if (cutToSets(param_des) != param_des.size()) {std::cerr << "Input size estimation interrupted: couldn't cut some predict dataset\r\n"; return false;}

    #if OUTPUT_LEVEL >= MEDIUM_OUTPUT
        std::clog << "Setting their names and purging target values\r\n";
    #endif
    for (size_t i=0; i<check_qtty; i++)
    {
        s.str("");
        s1.str("");
        s<<prefix_desired<<i;
        s1<<prefix_check<<i;
        cloneSet(s.str(), s1.str());
        if (!purgeTargets(s.str())) {std::cerr << "Input size estimation interrupted: cannot prepare test data\r\n"; return false;};
    }
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Datasets for prediction and checking ready.\r" << std::endl;
    #endif
    return true;

}

bool MLEnv::estimateDataSizeForAccuracy()
{
    #if OUTPUT_LEVEL >= LOW_OUTPUT
        std::clog << "Entering procedure: estimation of correlation between input size and method accuracy\r\n";
    #endif
    m_statistics["general"]="in progress";

    m_method=new VKF_caller();

    size_t enoughIterations=(m_method->isProbabilistic()?10:1);
    size_t check_qtty=1;/*intentionally. why have many sets for checking? one is enough. Can be connected to enoughIterations*/
    size_t min_input_size=2;
    size_t max_input_size=20;

    std::string prefix_input="input/";
    std::string prefix_desired="desired/";
    std::string prefix_check="check/";

    std::string predict="predict.tmp", learn="learn.tmp", result="result.tmp";
    std::fstream predictf(predict, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    std::fstream learnf(learn, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);
    std::fstream resultf(result, std::ios::out | std::ios::in | std::ios::binary | std::ios::trunc);

    std::ostringstream s, s1;

    if (!DataSizeForAcc_GeneratePredictAndCheck(prefix_desired, prefix_check, check_qtty)) return false;

    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Sending the only desired dataset to its file\r\n";
    #endif
    predictf.close();
    predictf.open(predict);
    s.str("");
    s<<prefix_desired<<0;
    sendSetToFile(s.str(), predictf);
    predictf.close();

    {
            /*
            use this sometime

            float progress = 0.0;
            while (progress < 1.0)
            {
                int barWidth = 70;

                std::cout << "[";
                int pos = barWidth * progress;
                for (int i = 0; i < barWidth; ++i)
                {
                    if (i < pos) std::cout << "=";
                    else if (i == pos) std::cout << ">";
                    else std::cout << " ";
                }
                std::cout << "] " << int(progress * 100.0) << " %\r";
                std::cout.flush();

                progress += 0.16; // for demonstration only
            }
            std::cout << "\r\n";

            or file:///C:/boost/boost_1_55_0/libs/timer/doc/original_timer.html#Class%20progress_display
            */
    }

    std::vector<std::vector<float> > accuracy;
    std::vector<float> tmp(enoughIterations);
    for (size_t j=0; j<enoughIterations; j++)
        tmp[j]=0;
    for (size_t i=min_input_size; i<max_input_size; i++)
    {
        accuracy.push_back(std::vector<float>(tmp));
    }

    for (size_t i=0; i<enoughIterations; i++)
    {
        deleteSetsByPrefix(prefix_input);

        #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
            std::clog << "Starting calling method\r" << std::endl;
        #endif

        for (size_t j=min_input_size; j<max_input_size; j++)
        {
            if (!DataSizeForAcc_GenerateLearnSet(prefix_input, j)) return false;
            learnf.close();
            learnf.open(learn);
            s.str("");
            s<<prefix_input<<j;
            sendSetToFile(s.str(), learnf);
            learnf.close();

            //std::this_thread::sleep_for(std::chrono::seconds(3));
            VKFit(learn, predict, result);

            eraseSet("file");
            resultf.close();
            resultf.open(result);
            if (!resultf or resultf.eof()) {std::cerr << "No result file" << "\r\n";continue;}
            else
                {addDescriptionsFromMachineFile(resultf, "file");}
            resultf.close();


            s.str("");
            s<<prefix_check<<0;//only one for now
            #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                printHumanInput("file", false);
                printHumanInput(s.str(), false);
            #endif
            if (!checkValues(s.str(), "edible", "file"))
            {
                std::clog << "Error comparing values, don't believe this statistics anymore!" << std::endl;
            }

            #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
                printStatistics();
            #endif // OUTPUT_LEVEL
            if (m_statistics["general"]!="uncompareable")
            {
                #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
                    std::cout << "Accuracy for dataset " << j << ":" << i << " = " << atof(m_statistics["correct predictions share"].c_str()) << std::endl;
                #endif
                accuracy[j-min_input_size][i]=atof(m_statistics["correct predictions share"].c_str());
            }
            else std::clog << "Error comparing values, don't believe this statistics anymore!" << std::endl;
            m_statistics["general"]="uncompareable";
        }
    }
    m_statistics["general"]="uncompareable";

    deleteSetsByPrefix(prefix_input);
    deleteSetsByPrefix(prefix_desired);
    deleteSetsByPrefix(prefix_check);
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::cout << "Total datasets after input size estimation's cleanup:" << m_data.size() << "\r\n";
    #endif
    return DataSizeForAcc_adviseSize(accuracy, min_input_size);
}

bool MLEnv::DataSizeForAcc_adviseSize(std::vector<std::vector<float> > accuracy, size_t min_input_size)
{
    float fiftyfifty=0.5;
    size_t closest_i=0;
    float closest_dist=std::numeric_limits<size_t>::max();
    for (size_t i=0; i<accuracy.size(); i++)
    {
        float dist=0;
        for (size_t j=0; j<accuracy[i].size(); j++)
        {
            #if OUTPUT_LEVEL >= MEDIUM_OUTPUT
                std::cout << i+min_input_size << " : " << accuracy[i][j] << std::endl;
            #endif
            dist+=fabs(fiftyfifty-accuracy[i][j]);
        }
        dist/=float(accuracy[i].size());
        //std::cout << dist << std::endl;
        if (dist<closest_dist) {closest_dist=dist; closest_i=i;}
    }
    std::cout << "Usage of input of size " << closest_i+min_input_size << " recommended for best approximation to "<< fiftyfifty << std::endl;
    return true;
}

void MLEnv::deleteSetsByPrefix(std::string prefix)
{
    std::map<std::string, ObjDescription *>::iterator i=m_data.begin();
    while (i!=m_data.end())//TODO: doesnt work
    {
        //std::clog << it.first << "\r\n";
        if (i->first.length()>prefix.length() and i->first.substr(0, prefix.length())==prefix)
        {
      //      st    d::clog << "deleting set " << i->first << " from existing " << m_data.size() <<" sets"<< "\r\n";
            i=eraseSet(i->first);
        }
        else ++i;
    }
}

bool MLEnv::deleteSet(std::string setname)
{
    //careful: cannot be used in cycles - invalidates iterators
    if (eraseSet(setname)!=m_data.end())
    {
        m_data.erase(setname);
        return true;
    }
    return false;
}

void MLEnv::findDependenciesWithOuterPredictor()
{
    //iterate on attributes:
        //set target to this attribute
        //iterate on sets (predefined number. its statistics after all):
            //get set of such size
            //vkf
            //compare, hashmap: what's the preciseness with such target
        //if preciseness statistically high
            //remember vkf hypothesys (?)
            //cout

    //somehow  use results of dependency analysis in creating other encoders
    //find whether preciseness increases
    //remember encoders ratings
}
void MLEnv::setAllValues(std::string dataset, std::string attr, std::string human_value)
{
    if (m_data.find(dataset)!=m_data.end())
    {
        size_t start;
        std::string nval=m_encoder->howToEncode(attr, human_value, start);
        m_data[dataset]->modifyAllValues(nval, start);
    }
    else std::cerr << "No such dataset" << "\r\n";
}

void MLEnv::reverseVal(std::string dset, size_t attr)
{
    if (m_data.find(dset)!=m_data.end())
    {
        m_data[dset]->reverseVal(m_encoder->supposeEncLength()-attr-1);
    }
    else std::cerr << "No such dataset" << "\r\n";
}
