#include "ObjDescription_dbs.h"
#include <cmath>
#include <boost/regex.hpp>
ObjDescription_dbs::ObjDescription_dbs()
{
    //ctor
}

ObjDescription_dbs::~ObjDescription_dbs()
{
    //dtor
}

/*ObjDescription_dbs::ObjDescription_dbs(const ObjDescription_dbs& other)
{
    m_data(other.m_data);
    //copy ctor
}*/

/*ObjDescription_dbs& ObjDescription_dbs::operator=(const ObjDescription_dbs& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}*/

size_t ObjDescription_dbs::addObject(boost::dynamic_bitset<> desc)
{
    m_data.push_back(desc);
    return m_data.size()-1;
}

size_t ObjDescription_dbs::addObject(std::string desc)
{
//    std::clog << "Creating object from string " << desc << "\r\n";
    boost::smatch m;
    if (boost::regex_search(desc, m, boost::regex("[^01]")))
    {
        std::cerr << "Cannot add object: is not a correct bitset\r" << std::endl;
        return 0;
    }
    if (m_data.size()>0)
    {
        #if OUTPUT_LEVEL>=ULTRA_OUTPUT
            std::clog << "Adding a piece of data to a dataset: checking whether it looks like already existing here bitsets"
        #endif
        if (desc.length()!=descriptionLength()) {std::cerr << "Cannot add object: incorrect length of bitset\r" << std::endl;return 0;}
    }

    m_data.push_back(boost::dynamic_bitset<>(desc));
    return m_data.size()-1;
}

size_t ObjDescription_dbs::addObject(ObjDescription * orig_set, size_t orig_index)
{
    m_data.push_back(orig_set->getObject_dbs(orig_index));
    return m_data.size()-1;
}

unsigned int ObjDescription_dbs::deleteObject(size_t index)
{
    if (index>=m_data.size()) return 1;
    m_data.erase(m_data.begin()+index);
    return 0;
}

std::string ObjDescription_dbs::getObject_str(size_t obj)
{
    std::string res;
    boost::to_string(getObject_dbs(obj), res);
    return res;
}

boost::dynamic_bitset<> ObjDescription_dbs::getObject_dbs(size_t obj)
{
    if (obj>m_data.size())
    {
        std::cerr << "No element with such index" << "\r\n";
        return boost::dynamic_bitset<>((m_data.size()>0?m_data[0].size():1),0);
    }
    return m_data[obj];
}

std::string ObjDescription_dbs::getValue(size_t obj, size_t attr)
{
    if (obj>m_data.size())
    {
        std::cerr << "No element with such index" << "\r\n";
        return "0";
    }
    if (attr>m_data[obj].size())
    {
        std::cerr << "No attribute with such index" << "\r\n";
        return "0";
    }

    std::string res;
    boost::to_string(getObject_dbs(obj), res);
    return res.substr(attr, 1);
}

size_t ObjDescription_dbs::size()
{
    return m_data.size();
}

size_t ObjDescription_dbs::intersectPositive(size_t object, boost::dynamic_bitset<> &res)
{
    /*if (attr_bit_1<attr_bit_0 or attr_bit_1>m_data[0].size() or object_1>attr_bit_1)
    {
        std::cerr << "Cannot intersect data by these bits... BECAUSE REASONS." << "\r\n";
    }
    size_t object=attr_bit_1-attr_bit_0-object_1;//dynamic bitset bits are numbered from least significant
    boost::dynamic_bitset<> res (attr_bit_1-attr_bit_0+1, (pow(2,attr_bit_1-attr_bit_0+1)-1));*/
//cannot use pow - overflow sometimes :)
    //std::clog << m_data[0].size() << ", initial res " << res <<"\r\n";
    bool initialres=true;
    size_t positives=0;
    for (size_t i=0; i<m_data.size(); i++)
    {
        if (m_data[i][object]==1)
        {
            /*for (size_t j=0; j<=m_data[0].size(); j++)
            {
                res[j]&=m_data[i][j];
            }*/
            if (!initialres) res&=m_data[i];
            else {res=m_data[i]; initialres=false;}
            std::clog << "with " << i << ", object" << object << ": " << res << "\r\n";
            positives++;
        }
    }
    return positives;
}

void ObjDescription_dbs::modifyAllValues(std::string nval_s, size_t start)
{
    if (m_data.size()==0)
        {std::cerr << m_data.size() << "No data yet!" << "\r\n"; return;}
    if (m_data[0].size()<=start)
        {std::cerr << "No bit N" << start << " for massmodification." << "\r\n"; return;}
    if (nval_s =="") {std::cerr << "Aborting mass modification: empty substitution asked\r" << std::endl; return;}

    boost::dynamic_bitset<> nval(nval_s);

    for (size_t i=0; i<m_data.size(); i++)
        for (size_t j=start; j<start+nval.size(); j++)
            {
                m_data[i][j]=nval[j-start];
            }
}

CompResult ObjDescription_dbs::check(ObjDescription * obj, size_t obj_i, size_t index)
{
    boost::dynamic_bitset<> o=obj->getObject_dbs(obj_i);
    if (index>m_data.size()-1) {std::cerr << "No such element" << "\r\n"; return CompResult::Error;}
    if (m_data[index]==o) return CompResult::Okay;
    if (m_data[index]<o) return CompResult::ShouldVBeenOkay;
    return CompResult::ShouldntVBeenOkay;
}

CompResult ObjDescription_dbs::check(ObjDescription * obj, size_t obj_i, size_t index, size_t fbit, size_t lbit)
{
    boost::dynamic_bitset<> o=obj->getObject_dbs(obj_i);
    std::string str;
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        boost::to_string(o, str);
        std::clog << "Comparing " << getObject_str(index) << " and " << str << "\r\n";
    #endif // OUTPUT_LEVEL
    if (index>m_data.size()-1 or fbit>lbit or lbit>descriptionLength()) {std::cerr << "No such element" << "\r\n"; return CompResult::Error;}
    for (int i=lbit; i>=fbit && i>=0/*YES IT IS NECESSARY because of fucking signed-unsigned comparison and YES I cannot make i size_t because it underflows then*/; --i)
    {
        if (m_data[index][i]<o[i-fbit])
            {
                #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                    std::clog << "real " << m_data[index][i] << " < " << o[i-fbit] << "\r\n";
                #endif // OUTPUT_LEVEL
                return CompResult::ShouldntVBeenOkay;
             }
        if (m_data[index][i]>o[i-fbit])
            {
                #if OUTPUT_LEVEL >= ULTRA_OUTPUT
                    std::clog << "real " << m_data[index][i] << " > " << o[i-fbit] << "\r\n";
                #endif
                return CompResult::ShouldVBeenOkay;}
    }
    #if OUTPUT_LEVEL >= ULTRA_OUTPUT
        std::clog << "okay" << "\r\n";
    #endif
    return CompResult::Okay;
}

void ObjDescription_dbs::reverseVal(size_t attr)
{
    if (m_data.size()==0)
        {std::cerr << m_data.size() << "No data yet!" << "\r\n"; return;}
    if (attr>m_data[0].size())
        {std::cerr << "No such attribute, cannot reverse value\r"<<std::endl; return;}

    for (size_t i=0; i<m_data.size(); i++)
        m_data[i][attr]=!m_data[i][attr];
}
