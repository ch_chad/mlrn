#ifndef MLMETHOD_ID3_H
#define MLMETHOD_ID3_H
#include "ObjDescription.h"

class MLMethod_ID3
{
    public:
        MLMethod_ID3();
        ~MLMethod_ID3();
        virtual void proceed (ObjDescription* in, ObjDescription* out);
    protected:
    private:
};

#endif // MLMETHOD_ID3_H
