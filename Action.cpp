#include "Action.h"
//#include <stringstream>
Action::Action(matcher a, matcher b, int (*func_pre)(std::vector<std::string>), std::vector<std::string> params_pre, int (*func_main)(std::vector<std::string>), std::string help):\
m_cmd(a), m_i(b), f_pre(func_pre), f_main(func_main), p_pre(params_pre), help_msg(help) {}

bool Action::argsMatched(bool interactivemode, std::string txt_cmd, boost::smatch m)
{
        #if OUTPUT_LEVEL >=VERBOSE_OUTPUT
            std::clog << (interactivemode?interactive.str():cmd.str()) << " probably is the option that we need, trying to parse its arguments\r" << std::endl;
        #endif
        if ((interactivemode?m_i.args[0]==0:m_cmd.args[0]==0))
        {
            #if OUTPUT_LEVEL>=ULTRA_OUTPUT
                std::clog << "No arguments needed\r" << std::endl;
            #endif // OUTPUT_LEVEL
            return true;
        }
        //for (auto i: m) std::cout <<"[" << i << "]";
        for (size_t i=0; (interactivemode?i<m_i.args.size():i<m_cmd.args.size()); i++)
        {
            size_t j=(interactivemode?m_i.args[i]:m_cmd.args[i]);
            if (j>=m.size())
            {
                #if OUTPUT_LEVEL>=VERBOSE_OUTPUT
                    std::clog << "Incorrect quantity of arguments\r" << std::endl;
                #endif // OUTPUT_LEVEL
                if (interactivemode) m_args_i.clear();
                else m_args_cmd.clear();
                return false;
            }
            if (interactivemode) m_args_i.push_back(m[j]);
            else m_args_cmd.push_back(m[j]);
        }
        #if OUTPUT_LEVEL>=VERBOSE_OUTPUT
            std::clog << "Parsed arguments:\r" << std::endl;
            for (size_t i=0; (interactivemode?i<m_i.args.size():i<m_cmd.args.size()); i++) std::clog << (interactivemode?m_i.args[i]:m_cmd.args[i]) << std::endl;
        #endif
        return true;

}

bool Action::match_cmd(std::string txt_cmd)
{
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Matching '" << txt_cmd << "' with " ;
        std::clog << cmd.str() << "\r" << std::endl;
    #endif // OUTPUT_LEVEL
    boost::smatch m;
    m_args_cmd.clear();
    if (boost::regex_match(txt_cmd, m, m_cmd.re))
        return argsMatched(false, txt_cmd, m);

    return false;
}

bool Action::match_interactive(std::string txt_cmd)
{
    boost::smatch m;
    m_args_i.clear();
    #if OUTPUT_LEVEL >= VERBOSE_OUTPUT
        std::clog << "Matching " << txt_cmd << " with " ;
        std::clog << interactive.str() << "\r" << std::endl;
    #endif // OUTPUT_LEVEL
    if (boost::regex_match(txt_cmd, m, m_i.re))
    {
        return argsMatched(true, txt_cmd, m);
    }
    return false;
}
