#ifndef globals_main_h
#define globals_main_h
#include "Action.h"
#include "MLEnv.h"
extern std::vector<Action> menu;
extern MLEnv * env;
void dummyEncoding(std::string enc_fname);
void fixSlashes(std::string &fname_enc_dirty);
#endif
