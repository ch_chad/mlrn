#ifndef ANALYZER_H
#define ANALYZER_H
#include <string>
#include "ObjDescription.h"

class Analyzer
{
    public:
        virtual ~Analyzer();
        virtual void prepare()=0;
        virtual void stop()=0;
        virtual std::string sumup(ObjDescription* result, ObjDescription* desired)=0;
    protected:
    private:
        Analyzer();
        Analyzer(const Analyzer& a);
};

#endif // ANALYZER_H
