#ifndef ENCODINGTABLE_H
#define ENCODINGTABLE_H

class EncodingCover
{
    private:
    std::vector<Attribute *> m_attrs;
};

class EncodingTable
{
    public:
        EncodingTable();
        ~EncodingTable();
        EncodingTable(const EncodingTable& other);
    protected:
    private:
        std::vector<EncodingCover> m_levels;
};

#endif // ENCODINGTABLE_H
