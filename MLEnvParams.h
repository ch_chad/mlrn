#ifndef MLENVPARAMS_H
#define MLENVPARAMS_H
#include "DataPart.h"
#include <fstream>
//#include "main.h"

#include "text_const.h"
class MLEnvParams
{
    public:
        MLEnvParams(std::string defaults=erroneous, std::string enc=erroneous, std::string input="");
        ~MLEnvParams();
        std::string enc(){std::fstream chk(enc_fname); if (enc_fname==erroneous or !chk) {dummyEnc();} return enc_fname;};
        std::string input(){std::fstream chk(input_fname); if (input_fname==erroneous or !chk) {dummyInput();} return input_fname;};
        std::string cutfile(){return cutparam_fname;};
        void dummyEnc(std::string fname) {enc_fname=fname; dummyEnc();};
    protected:
    private:
/*    static const std::string erroneous;
    static const std::string def_enc;
    static const std::string def_in;
    static const std::string def_cut;
*/    MLEnvParams();
    std::string setting_fname=erroneous;
    std::string enc_fname=erroneous;
    std::string input_fname=erroneous;
    std::string cutparam_fname=erroneous;
    std::vector <DataPart> cut_params;
    void dummyParams();
    void dummyEnc();
    void dummyInput();
    void dummyCut();
};

#endif // MLENVPARAMS_H
