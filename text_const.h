#ifndef TEXT_CONST_H_INCLUDED
#define TEXT_CONST_H_INCLUDED

const std::string def_settings="settings.txt";
const std::string erroneous="DEADBEEF";
const std::string def_enc="default_encoding.txt";
const std::string def_in="default_input.txt";
const std::string def_cut="default_cutparams.txt";


#endif // TEXT_CONST_H_INCLUDED
