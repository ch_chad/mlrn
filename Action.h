#ifndef ACTION_H
#define ACTION_H
#include "output_levels.h"
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <iostream>

struct matcher
{
        std::string re_txt;
        std::vector<int> args;
        boost::regex re;
        matcher(std::string a, std::string b, std::vector<int> c):re_txt(b), args(c), re("")
        {
                std::stringstream ss;
                ss << "^" << a << " *$";
                re=boost::regex(ss.str());
        };
};


class Action
{
    public:
        Action(matcher cmd, matcher inter,
               int (*func_pre)(std::vector<std::string>), std::vector<std::string> params_pre, \
               int (*func_main)(std::vector<std::string>), std::string help);
        bool match_cmd(std::string cmd);
        bool match_interactive(std::string cmd);
        int start_cmd()
        {
                int r=f_pre(p_pre);
                if (r!=0) return r;
                return f_main(m_args_cmd);
        };
        int start() {
                return f_main(m_args_i);
        };
        std::string help() {return help_msg;};
        std::string toString() {std::stringstream s; s << "\t* " << m_i.re_txt << " : " << help_msg; return s.str();};
    protected:
    private:
        bool argsMatched(bool interactivemode, std::string txt_cmd, boost::smatch m);
        //boost::smatch m;
        matcher m_cmd;
        matcher m_i;
        std::vector<std::string> m_args_cmd;//TODO: i strongly suspect just one arg set is sufficient... i'll think about it tomorrow :)
        std::vector<std::string> m_args_i;
        int (*f_pre)(std::vector<std::string> params);//preliminary called if started from cmd
        int (*f_main)(std::vector<std::string> params);//called anyway
        std::vector<std::string> p_pre;
        std::string help_msg;
        Action();
        Action& operator=(const Action&);
};

#endif // ACTION_H
