#ifndef TREENODE_H
#define TREENODE_H
#include <string>
#include "boost/dynamic_bitset.hpp"
#include "output_levels.h"
//#include "Encoder.h"

class treenode
{
    public:
        std::string text() {return value;};
        boost::dynamic_bitset<> enc_dbs() {return enc;};
        std::string enc_str() {std::string res; boost::to_string(enc, res); return res;};
        treenode(std::string v);
        treenode * newChild(std::string v) {return addChild(new treenode(v));};
        treenode * addChild(treenode * c);
//        int disconnectChild(treenode * c);
        int orphanize(treenode * c);
//        int disconnectChild(std::string v) {return disconnectChild(findInDescendants(v));}
        std::string toString(){return toString(0);}
        std::string toString(size_t tab_qtty);
        treenode * findInDescendants(std::string obj);
        size_t countLeaves();
        void generateEncodings(size_t bits, std::vector<std::string> &bit_descriptors);
        std::vector<std::string> getNames(){return names;};
    protected:
    private:
        std::vector<std::string> names;
//        Encoder* prnt;
        treenode();
        treenode(const treenode&);
        const treenode * operator=(const treenode &rhs);
        treenode * bro=NULL;
        treenode * fchild=NULL;
        treenode * parent=NULL;
        std::string value="NONE";
        boost::dynamic_bitset<> enc;
        size_t leavesHereAndBelow=0;
        void generateEncodings_internal(size_t length, std::string tabs, treenode * root, std::vector<std::string> &bit_descriptors);
};

#endif // TREENODE_H
