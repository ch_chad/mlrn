#ifndef OUTPUT_LEVEL
    #define SILENT_OUTPUT 0 //nothing unless asked for
    #define LOW_OUTPUT 1 //"starting calculations"
    #define MEDIUM_OUTPUT 2 //"starting step 1 of calculation"
    #define VERBOSE_OUTPUT 3 //"analyzing object #1"
    #define ULTRA_OUTPUT 4 //"analyzing object 1100001011"

    #define OUTPUT_LEVEL MEDIUM_OUTPUT
#endif
